/*
 * Copyright (c) 2019.
 * Javier Azcurra - jazcurra.it@gmail.com
 */

package com.company.island.service;

import org.apache.commons.lang3.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import static org.junit.Assert.*;

/**
 * @author Javier Azcurra
 * <p>
 * Test of {@link DateService}
 */

@RunWith(SpringRunner.class)
public class DateServiceTest {

    private final static int MIN_DAYS = 1, MAX_DAYS = 10;

    @InjectMocks
    DateService dateService;

    @Before
    public void setUp() throws Exception {
        int randomNum = new Random().nextInt((MAX_DAYS - MIN_DAYS) + 1) + MIN_DAYS;
        ReflectionTestUtils.setField(dateService, "minDaysAheadReserve", randomNum);
        randomNum = new Random().nextInt((3 - MIN_DAYS) + 1) + MIN_DAYS;
        ReflectionTestUtils.setField(dateService, "maxMonthsArrival", randomNum);
        randomNum = new Random().nextInt((MAX_DAYS - MIN_DAYS) + 1) + MIN_DAYS;
        ReflectionTestUtils.setField(dateService, "maxDaysReserve", randomNum);
    }

    private static Calendar getCalendarTruncated() {
        Calendar c = Calendar.getInstance();
        c.setTime(DateUtils.truncate(new Date(), Calendar.DATE));
        return c;
    }

    @Test
    public void testGetMinDateDepartureOfArrival() {
        Calendar c = getCalendarTruncated();
        c.add(Calendar.DATE, 1);
        Date d = dateService.getMinDateDepartureOfArrival(getCalendarTruncated().getTime());
        assertEquals(c.getTime().getTime(), d.getTime());
    }

    @Test
    public void testGetMaxDateDepartureOfArrival1() {
        Calendar c = getCalendarTruncated();
        c.add(Calendar.DATE, dateService.getMaxDaysReserve());
        Date d = dateService.getMaxDateDepartureOfArrival(getCalendarTruncated().getTime());
        assertEquals(c.getTime().getTime(), d.getTime());
    }

    @Test
    public void testGetMaxDateDepartureOfArrival2() {
        Calendar c = getCalendarTruncated();
        c.setTime(dateService.getMaxDateOfDeparture());
        c.add(Calendar.DATE, 4);
        Date d = dateService.getMaxDateDepartureOfArrival(c.getTime());
        assertEquals(dateService.getMaxDateOfDeparture().getTime(), d.getTime());
    }

    @Test
    public void testGetLocalDateMinDaysAheadReserveOK() {
        Date d = dateService.getMinDateOfArrival();
        Calendar c = getCalendarTruncated();
        c.add(Calendar.DATE, dateService.getMinDaysAheadReserve());
        assertEquals(0, d.getHours());
        assertEquals(0, d.getMinutes());
        assertEquals(0, d.getSeconds());
        assertEquals(d, c.getTime());
    }

    @Test
    public void testGetLocalDateMaxMonthsArrivalOK() {
        Date d = dateService.getMaxDateOfArrival();
        Calendar c = getCalendarTruncated();
        c.add(Calendar.MONTH, dateService.getMaxMonthsArrival());
        c.add(Calendar.DATE, -1);
        assertEquals(0, d.getHours());
        assertEquals(0, d.getMinutes());
        assertEquals(0, d.getSeconds());
        assertEquals(d, c.getTime());
    }

    @Test
    public void testArrivalAfterMinTimeReserveOK() {
        Calendar c2 = getCalendarTruncated();
        c2.add(Calendar.DATE, dateService.getMinDaysAheadReserve() + MIN_DAYS);
        assertTrue(dateService.checkArrivalAfterMinTimeReserve(c2.getTime()));
    }

    @Test
    public void testArrivalAfterMinTimeReserveFail() {
        Calendar c2 = getCalendarTruncated();
        c2.add(Calendar.MONTH, dateService.getMaxMonthsArrival() - (MAX_DAYS + 1));
        assertFalse(dateService.checkArrivalAfterMinTimeReserve(c2.getTime()));
    }


    @Test
    public void testCheckDepartureBeforeMaxTimeReserveOK() {
        Calendar c2 = getCalendarTruncated();
        c2.add(Calendar.DATE, dateService.getMinDaysAheadReserve() + MIN_DAYS);
        assertTrue(dateService.checkDepartureBeforeMaxTimeReserve(c2.getTime()));
    }

    @Test
    public void testCheckDepartureBeforeMaxTimeReserveFail() {
        Calendar c2 = getCalendarTruncated();
        c2.add(Calendar.MONTH, dateService.getMaxMonthsArrival() + MIN_DAYS);
        assertFalse(dateService.checkDepartureBeforeMaxTimeReserve(c2.getTime()));
    }

    @Test
    public void testCheckBetweenArrivalDepartureDiffMaxDaysOK1() {
        Calendar c1 = getCalendarTruncated();
        c1.add(Calendar.DATE, dateService.getMinDaysAheadReserve());
        Calendar c2 = getCalendarTruncated();
        c2.add(Calendar.DATE, dateService.getMinDaysAheadReserve() + dateService.getMaxDaysReserve());
        assertTrue(dateService.checkBetweenArrivalDepartureDiffMaxDays(c1.getTime(), c2.getTime()));
    }

    @Test
    public void testCheckBetweenArrivalDepartureDiffMaxDaysOK2() {
        Calendar c1 = getCalendarTruncated();
        c1.add(Calendar.DATE, dateService.getMinDaysAheadReserve() + MIN_DAYS);
        Calendar c2 = getCalendarTruncated();
        c2.add(Calendar.DATE, dateService.getMinDaysAheadReserve() + (MIN_DAYS * 2));
        assertTrue(dateService.checkBetweenArrivalDepartureDiffMaxDays(c1.getTime(), c2.getTime()));
    }

    @Test
    public void testCheckBetweenArrivalDepartureDiffMaxDaysFail1() {
        Calendar c1 = getCalendarTruncated();
        c1.add(Calendar.DATE, dateService.getMinDaysAheadReserve() + 1);
        Calendar c2 = getCalendarTruncated();
        c2.add(Calendar.DATE, dateService.getMinDaysAheadReserve() + (MAX_DAYS * 2));
        assertFalse(dateService.checkBetweenArrivalDepartureDiffMaxDays(c1.getTime(), c2.getTime()));
    }

    @Test
    public void testCheckBetweenArrivalDepartureDiffMaxDaysFail2() {
        Calendar c1 = getCalendarTruncated();
        c1.add(Calendar.DATE, dateService.getMinDaysAheadReserve() - MAX_DAYS);
        Calendar c2 = getCalendarTruncated();
        c2.add(Calendar.DATE, dateService.getMinDaysAheadReserve() + MAX_DAYS);
        assertFalse(dateService.checkBetweenArrivalDepartureDiffMaxDays(c1.getTime(), c2.getTime()));
    }

    @Test
    public void testCheckBetweenArrivalDepartureDiffMaxDaysFail3() {
        Calendar c1 = getCalendarTruncated();
        c1.add(Calendar.DATE, dateService.getMinDaysAheadReserve() + MAX_DAYS);
        Calendar c2 = getCalendarTruncated();
        c2.add(Calendar.DATE, dateService.getMinDaysAheadReserve() - MAX_DAYS);
        assertFalse(dateService.checkBetweenArrivalDepartureDiffMaxDays(c1.getTime(), c2.getTime()));
    }

    @Test
    public void testCheckArrivalBeforeDepartureOK() {
        Calendar c1 = getCalendarTruncated();
        c1.add(Calendar.DATE, 1);
        Calendar c2 = getCalendarTruncated();
        c2.add(Calendar.DATE, 2);
        assertTrue(dateService.checkArrivalBeforeDeparture(c1.getTime(), c2.getTime()));
    }

    @Test
    public void testCheckArrivalBeforeDepartureFail() {
        Calendar c1 = getCalendarTruncated();
        c1.add(Calendar.DATE, 3);
        Calendar c2 = getCalendarTruncated();
        c2.add(Calendar.DATE, 1);
        assertFalse(dateService.checkArrivalBeforeDeparture(c1.getTime(), c2.getTime()));
    }
}