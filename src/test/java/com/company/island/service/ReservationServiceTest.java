/*
 * Copyright (c) 2019.
 * Javier Azcurra - jazcurra.it@gmail.com
 */

package com.company.island.service;

import com.company.island.builder.ReservationBuilder;
import com.company.island.dto.AvailabilityResponse;
import com.company.island.dto.ReservationRequest;
import com.company.island.entity.ReservationEntity;
import com.company.island.entity.UserEntity;
import com.company.island.exception.BadRequestException;
import com.company.island.exception.NotFoundException;
import com.company.island.repository.ReservationRepository;
import com.company.island.repository.UserRepository;
import com.company.island.validator.ReservationValidator;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.context.MessageSource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.validation.BindingResult;

import java.util.*;

import static junit.framework.TestCase.assertTrue;
import static junit.framework.TestCase.fail;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * @author Javier Azcurra
 * <p>
 * Test of {@link ReservationService}
 */

@RunWith(SpringRunner.class)
public class ReservationServiceTest {

    private static Date DATE_ARRIVAL, DATE_DEPARTURE;
    private static Date DATE_ARRIVAL2, DATE_DEPARTURE2;

    private final static String FULL_NAME = "testName";
    private final static String EMAIL = "test@mail.com";
    private final static String BOOKING_ID = "test_b1";

    private final static String BOOKING_ID2 = "test_b2";
    private final static String FULL_NAME2 = "test2Name";
    private final static String EMAIL2 = "test2@mail.com";

    private ReservationEntity reservationEntity;

    @InjectMocks
    private DateService dateService;

    @InjectMocks
    private ReservationService reservationService;

    @Mock
    private ReservationRepository reservationRepository;

    @Mock
    private ReservationRequest reservationRequest;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private ReservationValidator reservationValidator;

    private Date getDateFromArrival(final int plusDaysOfMin) {
        if (plusDaysOfMin > (dateService.getMaxMonthsArrival() * 30))
            fail("days must be less than maxMonthsArribal");

        Calendar c = Calendar.getInstance();
        c.setTime(DateUtils.truncate(new Date(), Calendar.DATE));
        c.add(Calendar.DATE, dateService.getMinDaysAheadReserve() + plusDaysOfMin);
        return c.getTime();
    }

    @Before
    public void setUp() throws Exception {
        ReflectionTestUtils.setField(reservationService, "reservationRepository", reservationRepository);
        ReflectionTestUtils.setField(reservationService, "dateService", dateService);
        ReflectionTestUtils.setField(reservationService, "userRepository", userRepository);
        ReflectionTestUtils.setField(reservationService, "reservationValidator", reservationValidator);
        ReflectionTestUtils.setField(reservationValidator, "messageSource", mock(MessageSource.class));
        ReflectionTestUtils.setField(reservationValidator, "dateService", dateService);
        ReflectionTestUtils.setField(dateService, "minDaysAheadReserve", 1);
        ReflectionTestUtils.setField(dateService, "maxMonthsArrival", 1);
        ReflectionTestUtils.setField(dateService, "maxDaysReserve", 3);

        DATE_ARRIVAL = getDateFromArrival(1);
        DATE_DEPARTURE = getDateFromArrival(2);
        DATE_ARRIVAL2 = getDateFromArrival(7);
        DATE_DEPARTURE2 = getDateFromArrival(9);

        ReservationBuilder reservationBuilder = new ReservationBuilder();
        reservationEntity = reservationBuilder
                .isCancelled(Boolean.FALSE)
                .withUser(new UserEntity(FULL_NAME, EMAIL))
                .withArrivalDate(DATE_ARRIVAL)
                .withDepartureDate(DATE_DEPARTURE)
                .withBookingId(BOOKING_ID)
                .build();
    }

    @Test
    public void testGetReservationOK() {
        when(reservationRepository.findByBookingId(BOOKING_ID)).thenReturn(reservationEntity);

        ReservationEntity reservationEntity = reservationService.getReservation(BOOKING_ID);
        assertNotNull(reservationEntity);
        assertEquals(FULL_NAME, reservationEntity.getUser().getFullName());
        assertEquals(EMAIL, reservationEntity.getUser().getEmail());
        assertEquals(BOOKING_ID, reservationEntity.getBookingId());
    }

    @Test
    public void testGetListReservationOK() {
        List<ReservationEntity> reservationEntityList = new ArrayList<ReservationEntity>() {{
            add(reservationEntity);
        }};

        when(reservationRepository.findAll()).thenReturn(reservationEntityList);
        List<ReservationEntity> rL = reservationService.searchReservation(null, null);
        assertNotNull(rL);
        assertFalse(rL.isEmpty());
        assertTrue(rL.get(0).equals(reservationEntity));
    }

    @Test
    public void testGetListReservationOK2() {
        List<ReservationEntity> reservationEntityList = new ArrayList<ReservationEntity>() {{
            add(reservationEntity);
        }};

        when(reservationRepository.findByUserFullNameStartingWithIgnoreCaseAndUserEmailStartingWith(BOOKING_ID, EMAIL)).thenReturn(reservationEntityList);
        List<ReservationEntity> rL = reservationService.searchReservation(BOOKING_ID, EMAIL);
        assertNotNull(rL);
        assertFalse(rL.isEmpty());
        assertTrue(rL.get(0).equals(reservationEntity));
    }

    @Test
    public void testGetListReservationOK3() {
        List<ReservationEntity> reservationEntityList = new ArrayList<ReservationEntity>() {{
            add(reservationEntity);
        }};

        when(reservationRepository.findByUserFullNameStartingWithIgnoreCaseAndUserEmailStartingWith(BOOKING_ID, EMAIL)).thenReturn(reservationEntityList);
        List<ReservationEntity> rL = reservationService.searchReservation(BOOKING_ID + "  ", "   " + EMAIL);
        assertNotNull(rL);
        assertFalse(rL.isEmpty());
        assertTrue(rL.get(0).equals(reservationEntity));
    }

    @Test(expected = NotFoundException.class)
    public void testGetReservationFails1() {
        when(reservationRepository.findByBookingId(BOOKING_ID2)).thenReturn(null);
        reservationService.getReservation(BOOKING_ID2);
        fail("NotFoundException must be thrown!");
    }

    @Test(expected = NotFoundException.class)
    public void testGetReservationFails2() {
        reservationEntity.setCancelled(Boolean.TRUE);
        reservationService.getReservation(BOOKING_ID);
        fail("NotFoundException must be thrown!");
    }

    @Test(expected = NotFoundException.class)
    public void testGetReservationFails3() {
        reservationService.getReservation("     ");
        fail("BadRequestException must be thrown!");
    }

    @Test(expected = NotFoundException.class)
    public void testGetReservationFails4() {
        reservationService.getReservation(null);
        fail("BadRequestException must be thrown!");
    }

    @Test
    public void testGetAvailabilityDateRangeOK1() {
        final Date minArrival = dateService.getMinDateOfArrival();
        final Date maxMonth = dateService.getMaxDateOfArrival();

        AvailabilityResponse availabilityResponse = reservationService.getAvailabilityDateRange(null, null);
        assertNotNull(availabilityResponse);

        assertEquals(minArrival, availabilityResponse.getStartDate());
        assertEquals(maxMonth, availabilityResponse.getEndDate());
    }

    @Test
    public void testGetAvailabilityDateRangeOK2() {
        final Calendar c = Calendar.getInstance();
        c.setTime(dateService.getMinDateOfArrival());
        c.add(Calendar.DATE, 2);

        final Date arrivalDateSelected = c.getTime();
        final Date start = dateService.getMinDateDepartureOfArrival(arrivalDateSelected);
        final Date end = dateService.getMaxDateDepartureOfArrival(arrivalDateSelected);

        AvailabilityResponse availabilityResponse = reservationService.getAvailabilityDateRange("deParTuRe", arrivalDateSelected);
        assertNotNull(availabilityResponse);

        assertEquals(start, availabilityResponse.getStartDate());
        assertEquals(end, availabilityResponse.getEndDate());
    }

    @Test(expected = BadRequestException.class)
    public void testGetAvailabilityDateRangeFail1() {
        reservationService.getAvailabilityDateRange("deParTuRe", null);
        fail("BadRequestException must be thrown!");
    }

    @Test(expected = BadRequestException.class)
    public void testGetAvailabilityDateRangeFail2() {
        reservationService.getAvailabilityDateRange("deParTuRe", new Date());
        fail("BadRequestException must be thrown!");
    }

    @Test(expected = BadRequestException.class)
    public void testGetAvailabilityDateRangeFail3() {
        Calendar c = Calendar.getInstance();
        c.setTime(DateUtils.truncate(new Date(), Calendar.DATE));
        c.add(Calendar.MONTH, dateService.getMaxMonthsArrival() * 2);
        reservationService.getAvailabilityDateRange("deParTuRe", c.getTime());
        fail("BadRequestException must be thrown!");
    }

    @Test
    public void testCancelReservationOK() {
        when(reservationRepository.findByBookingId(BOOKING_ID)).thenReturn(reservationEntity);
        when(reservationRepository.saveAndFlush(any())).thenReturn(reservationEntity);

        ReservationEntity reservationEntity = reservationService.cancelReservation(BOOKING_ID, Boolean.TRUE);
        assertNotNull(reservationEntity);
        assertTrue(reservationEntity.isCancelled());
    }

    @Test(expected = NotFoundException.class)
    public void testCancelReservationFAIL() {
        when(reservationRepository.findByBookingId(BOOKING_ID2)).thenReturn(null);
        reservationService.cancelReservation(BOOKING_ID2, Boolean.TRUE);
        fail("NotFoundException must be thrown!");
    }

    @Test(expected = NotFoundException.class)
    public void testCancelReservationFAIL2() {
        reservationEntity.setCancelled(Boolean.TRUE);
        when(reservationRepository.findByBookingId(BOOKING_ID2)).thenReturn(reservationEntity);
        reservationService.cancelReservation(BOOKING_ID2, Boolean.TRUE);
        fail("NotFoundException must be thrown!");
    }

    @Test(expected = BadRequestException.class)
    public void testCancelReservationFAIL3() {
        when(reservationRepository.findByBookingId(BOOKING_ID2)).thenReturn(reservationEntity);
        reservationService.cancelReservation(BOOKING_ID2, Boolean.FALSE);
        fail("NotFoundException must be thrown!");
    }

    @Test(expected = BadRequestException.class)
    public void testCancelReservationFAIL4() {
        when(reservationRepository.findByBookingId(BOOKING_ID2)).thenReturn(reservationEntity);
        reservationService.cancelReservation(BOOKING_ID2, null);
        fail("NotFoundException must be thrown!");
    }

    @Test
    public void testUpdateReservationOK() {
        when(reservationRepository.findByBookingId(BOOKING_ID)).thenReturn(reservationEntity);
        when(reservationRepository.saveAndFlush(reservationEntity)).thenReturn(reservationEntity);

        final Date newDateA = DATE_ARRIVAL2;
        final Date newDateD = DATE_DEPARTURE2;

        when(reservationRequest.getArrivalDate()).thenReturn(newDateA);
        when(reservationRequest.getDepartureDate()).thenReturn(newDateD);

        ReservationEntity reservationEntity = reservationService.updateReservation(BOOKING_ID, reservationRequest, mock(BindingResult.class));
        assertNotNull(reservationEntity);
        assertEquals(FULL_NAME, reservationEntity.getUser().getFullName());
        assertEquals(EMAIL, reservationEntity.getUser().getEmail());
        assertNotEquals(DATE_ARRIVAL, newDateA);
        assertNotEquals(DATE_DEPARTURE, newDateD);
        assertEquals(newDateA, reservationEntity.getArrivalDate());
        assertEquals(newDateD, reservationEntity.getDepartureDate());
    }

    @Test(expected = NotFoundException.class)
    public void testUpdateReservationFail() {
        when(reservationRepository.findByBookingId(BOOKING_ID2)).thenReturn(null);
        reservationService.updateReservation(BOOKING_ID2, reservationRequest, mock(BindingResult.class));
        fail("NotFoundException must be thrown!");
    }

    @Test(expected = NotFoundException.class)
    public void testUpdateReservationFail2() {
        reservationEntity.setCancelled(Boolean.TRUE);
        when(reservationRepository.findByBookingId(BOOKING_ID2)).thenReturn(reservationEntity);
        reservationService.updateReservation(BOOKING_ID2, reservationRequest, mock(BindingResult.class));
        fail("NotFoundException must be thrown!");
    }

    @Test
    public void testCreateReservationOK() {
        when(userRepository.findByEmail(anyString())).thenReturn(null);

        reservationService.createReservation(reservationRequest, mock(BindingResult.class));
        verify(userRepository, times(1)).saveAndFlush(any());
        verify(reservationRepository, times(1)).saveAndFlush(any());
    }

    @Test
    public void testCreateReservationOK2() {
        when(reservationRequest.getEmail()).thenReturn(EMAIL2);
        UserEntity userEntity = new UserEntity(FULL_NAME2, EMAIL2);
        ReflectionTestUtils.setField(userEntity, "id", 1L);

        when(userRepository.findByEmail(EMAIL2)).thenReturn(userEntity);
        when(reservationRequest.getFullName()).thenReturn(FULL_NAME);
        when(reservationRepository.findAllByUserId(1L)).thenReturn(Collections.EMPTY_LIST);


        reservationService.createReservation(reservationRequest, mock(BindingResult.class));
        verify(reservationRepository, times(1)).saveAndFlush(any());
        assertEquals(FULL_NAME2, userEntity.getFullName());
        assertEquals(EMAIL2, userEntity.getEmail());
    }

    @Test(expected = BadRequestException.class)
    public void testCreateReservationFail() {
        UserEntity userEntity = new UserEntity(FULL_NAME2, EMAIL2);
        ReflectionTestUtils.setField(userEntity, "id", 1L);

        when(userRepository.findByEmail(anyString())).thenReturn(userEntity);
        when(reservationRequest.getFullName()).thenReturn(FULL_NAME2);
        when(reservationRequest.getEmail()).thenReturn(EMAIL2);
        when(reservationRequest.getArrivalDate()).thenReturn(DATE_ARRIVAL2);
        when(reservationRequest.getDepartureDate()).thenReturn(DATE_DEPARTURE2);

        when(reservationRepository.findAllByUserId(1L)).thenReturn(Arrays.asList((reservationEntity)));

        reservationService.createReservation(reservationRequest, mock(BindingResult.class));
        fail("BadRequestException must be thrown!");
    }

    @Test
    public void testDeleteReservationOK() {
        when(reservationRepository.findByBookingId(BOOKING_ID)).thenReturn(reservationEntity);
        doNothing().when(reservationRepository).delete(reservationEntity);

        reservationService.deleteReservation(BOOKING_ID);
    }

    @Test(expected = NotFoundException.class)
    public void testDeleteReservationFAIL() {
        when(reservationRepository.findByBookingId(BOOKING_ID2)).thenReturn(null);
        reservationService.deleteReservation(BOOKING_ID2);
        fail("NotFoundException must be thrown!");
    }
}