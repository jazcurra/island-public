/*
 * Copyright (c) 2019.
 * Javier Azcurra - jazcurra.it@gmail.com
 */

package com.company.island.builder;

import com.company.island.entity.ReservationEntity;
import com.company.island.entity.UserEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author Javier Azcurra
 * <p>
 * Test of {@link ReservationBuilder}
 */

@RunWith(SpringRunner.class)
public class ReservationBuilderTest {

    private final static Date _ArrivalDate = new Date(new Random().nextInt());
    private final static Date _DepartureDate = new Date(new Random().nextInt());
    private final static Date _CreationDate = new Date(new Random().nextInt());
    private final static Date _UpdateDate = new Date(new Random().nextInt());
    private final static Date _CancelDate = new Date(new Random().nextInt());
    private final static boolean _IsActive = Boolean.TRUE;
    private final UserEntity _User = new UserEntity("test" + new Random().nextInt(), "test" + new Random().nextInt() + "@mail.com");

    @Test
    public void mainTest() {
        ReservationEntity reservationEntity = ReservationEntity.getBuilder()
                .withUser(_User)
                .withArrivalDate(_ArrivalDate)
                .withDepartureDate(_DepartureDate)
                .withCreationDate(_CreationDate)
                .withUpdateDate(_UpdateDate)
                .withCancelDate(_CancelDate)
                .isCancelled(_IsActive)
                .build();

        assertNotNull(reservationEntity);
        assertEquals(_User, reservationEntity.getUser());
        assertEquals(_ArrivalDate, reservationEntity.getArrivalDate());
        assertEquals(_DepartureDate, reservationEntity.getDepartureDate());
        assertEquals(_CreationDate, reservationEntity.getCreationDate());
        assertEquals(_UpdateDate, reservationEntity.getUpdateDate());
        assertEquals(_CancelDate, reservationEntity.getCancelDate());
        assertEquals(_IsActive, reservationEntity.isCancelled());
    }
}