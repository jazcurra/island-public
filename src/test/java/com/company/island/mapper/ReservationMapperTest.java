/*
 * Copyright (c) 2019.
 * Javier Azcurra - jazcurra.it@gmail.com
 */

package com.company.island.mapper;

import com.company.island.dto.ReservationRequest;
import com.company.island.dto.ReservationResponse;
import com.company.island.entity.ReservationEntity;
import com.company.island.entity.UserEntity;
import com.company.island.entity.enumeration.ReservationStatus;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author Javier Azcurra
 * <p>
 * Test of {@link ReservationMapper}
 */

@RunWith(SpringRunner.class)
public class ReservationMapperTest {

    private static String _BookingId1 = "bid" + new Random().nextInt();
    private static Date _ArrivalDate1 = new Date(new Random().nextInt());
    private static Date _DepartureDate1 = new Date(new Random().nextInt());
    private final UserEntity user1 = new UserEntity("test" + new Random().nextInt(), "test" + new Random().nextInt() + "@mail.com");
    private static boolean _IsCancel = Boolean.TRUE;

    @InjectMocks
    private ReservationMapper reservationMapper;

    private ReservationEntity reservationEntity;


    @Before
    public void setUp() throws Exception {
        reservationEntity = ReservationEntity.getBuilder()
                .withBookingId(_BookingId1)
                .withArrivalDate(_ArrivalDate1)
                .withDepartureDate(_DepartureDate1)
                .withUser(user1)
                .isCancelled(_IsCancel)
                .build();
    }

    @Test
    public void testConvertRequest() {
        ReservationRequest reservationRequest = reservationMapper.convertRequest(reservationEntity);
        assertNotNull(reservationRequest);
        assertEquals(reservationEntity.getUser().getFullName(), reservationRequest.getFullName());
        assertEquals(reservationEntity.getUser().getEmail(), reservationRequest.getEmail());
        assertEquals(reservationEntity.getArrivalDate(), reservationRequest.getArrivalDate());
        assertEquals(reservationEntity.getDepartureDate(), reservationRequest.getDepartureDate());
    }

    @Test
    public void testConvertResponse() {
        ReservationResponse reservationResponse = reservationMapper.convertResponse(reservationEntity);
        assertNotNull(reservationResponse);
        assertEquals(reservationEntity.getUser().getFullName(), reservationResponse.getFullName());
        assertEquals(reservationEntity.getUser().getEmail(), reservationResponse.getEmail());
        assertEquals(reservationEntity.getArrivalDate(), reservationResponse.getArrivalDate());
        assertEquals(reservationEntity.getDepartureDate(), reservationResponse.getDepartureDate());
        assertEquals(reservationEntity.getBookingId(), reservationResponse.getBookingId());
        Assert.assertEquals(ReservationStatus.CANCELLED.toString(), reservationResponse.getStatus());
    }
}