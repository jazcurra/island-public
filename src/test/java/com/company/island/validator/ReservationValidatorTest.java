/*
 * Copyright (c) 2019.
 * Javier Azcurra - jazcurra.it@gmail.com
 */

package com.company.island.validator;

import com.company.island.service.DateService;
import com.company.island.dto.ReservationRequest;
import com.company.island.exception.BadRequestException;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.context.MessageSource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.validation.BeanPropertyBindingResult;

import java.util.Calendar;

import static junit.framework.TestCase.*;
import static org.mockito.Mockito.mock;

/**
 * @author Javier Azcurra
 * <p>
 * Test of {@link ReservationValidator}
 */

@RunWith(SpringRunner.class)
public class ReservationValidatorTest {

    @InjectMocks
    private DateService dateService;

    @InjectMocks
    private ReservationValidator reservationValidator;

    private ReservationRequest reservationRequest;

    private BeanPropertyBindingResult beanPropertyBindingResult;

    @Before
    public void setUp() throws Exception {
        ReflectionTestUtils.setField(reservationValidator, "stringLengthMax", 90);
        ReflectionTestUtils.setField(reservationValidator, "messageSource", mock(MessageSource.class));
        ReflectionTestUtils.setField(reservationValidator, "dateService", dateService);
        ReflectionTestUtils.setField(dateService, "minDaysAheadReserve", 1);
        ReflectionTestUtils.setField(dateService, "maxMonthsArrival", 1);
        ReflectionTestUtils.setField(dateService, "maxDaysReserve", 3);
        beanPropertyBindingResult = new BeanPropertyBindingResult(reservationRequest, "reservationRequest");
        reservationRequest = new ReservationRequest();
    }

    @Test
    public void testSupports() {
        assertTrue(reservationValidator.supports(ReservationValidator.class));
    }

    @Test
    public void testValidateOK() {
        reservationValidator.validate(reservationRequest, beanPropertyBindingResult);
        assertTrue(beanPropertyBindingResult.getErrorCount() > 0);
    }

    @Test
    public void testValidateOK2() {
        Calendar c1 = Calendar.getInstance();
        c1.add(Calendar.MONTH, dateService.getMaxMonthsArrival() + 5);
        Calendar c2 = Calendar.getInstance();
        c2.add(Calendar.MONTH, dateService.getMaxMonthsArrival() + 3);
        reservationRequest.setFullName("  Full Name OK  ");
        reservationRequest.setEmail("   testOK@mail.com      ");
        reservationRequest.setArrivalDate(c1.getTime());
        reservationRequest.setDepartureDate(c2.getTime());
        reservationValidator.validate(reservationRequest, beanPropertyBindingResult);
        assertEquals("Full Name OK", reservationRequest.getFullName());
        assertEquals("testok@mail.com", reservationRequest.getEmail());
        assertTrue(beanPropertyBindingResult.getErrorCount() > 0);
    }

    @Test
    public void testValidateOK3() {
        Calendar c1 = Calendar.getInstance();
        c1.add(Calendar.MONTH, dateService.getMaxMonthsArrival() - 3);
        Calendar c2 = Calendar.getInstance();
        c2.add(Calendar.MONTH, dateService.getMaxMonthsArrival() - 5);
        reservationRequest.setFullName(StringUtils.repeat("*", 100));
        reservationRequest.setEmail("testOKmail.com");
        reservationRequest.setArrivalDate(c1.getTime());
        reservationRequest.setDepartureDate(c2.getTime());
        reservationValidator.validate(reservationRequest, beanPropertyBindingResult);
        assertEquals(StringUtils.repeat("*", 100), reservationRequest.getFullName());
        assertEquals("testOKmail.com", reservationRequest.getEmail());
        assertTrue(beanPropertyBindingResult.getErrorCount() > 0);
    }

    @Test
    public void testValidateOK4() {
        reservationRequest.setFullName("Full Name OK");
        reservationRequest.setEmail(StringUtils.repeat("*", 100));
        reservationValidator.validate(reservationRequest, beanPropertyBindingResult);
        assertEquals("Full Name OK", reservationRequest.getFullName());
        assertEquals(StringUtils.repeat("*", 100), reservationRequest.getEmail());
        assertTrue(beanPropertyBindingResult.getErrorCount() > 0);
    }

    @Test
    public void testValidateOK5() {
        Calendar c1 = Calendar.getInstance();
        c1.add(Calendar.DATE, -30);
        Calendar c2 = Calendar.getInstance();
        c2.add(Calendar.DATE, dateService.getMaxMonthsArrival() + 30);
        reservationRequest.setFullName(StringUtils.repeat("*", 100));
        reservationRequest.setEmail("testOKmail.com");
        reservationRequest.setArrivalDate(c1.getTime());
        reservationRequest.setDepartureDate(c2.getTime());
        reservationValidator.validate(reservationRequest, beanPropertyBindingResult);
        assertEquals(StringUtils.repeat("*", 100), reservationRequest.getFullName());
        assertEquals("testOKmail.com", reservationRequest.getEmail());
        assertTrue(beanPropertyBindingResult.getErrorCount() > 0);
    }

    @Test
    public void testValidateInputOK() {
        Calendar c1 = Calendar.getInstance();
        c1.add(Calendar.DATE, dateService.getMinDaysAheadReserve());
        Calendar c2 = Calendar.getInstance();
        c2.add(Calendar.DATE, dateService.getMinDaysAheadReserve() + dateService.getMaxDaysReserve());
        reservationRequest.setFullName("test name");
        reservationRequest.setEmail("test@mail.com");
        reservationRequest.setArrivalDate(c1.getTime());
        reservationRequest.setDepartureDate(c2.getTime());

        try {
            reservationValidator.validateInput(reservationRequest, beanPropertyBindingResult);
        } catch (BadRequestException e) {
            fail("Exception must not be thrown");
        }
    }

    @Test(expected = BadRequestException.class)
    public void testValidateInputFail() {
        reservationValidator.validateInput(reservationRequest, beanPropertyBindingResult);
    }
}