/*
 * Copyright (c) 2019.
 * Javier Azcurra - jazcurra.it@gmail.com
 */

package com.company.island.dto;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * @author Javier Azcurra
 * <p>
 * Test of {@link ReservationRequest}
 */

@RunWith(SpringRunner.class)
public class ReservationRequestTest {

    private static String FULL_NAME = "fullname";
    private static String EMAIL = "email";
    private static Date ARRIVAL_DATE = new Date(1234);
    private static Date DEPARTURE_DATE = new Date(4567);

    private ReservationRequest reservationRequest;

    @Before
    public void setUp() throws Exception {
        reservationRequest = new ReservationRequest();
    }

    @Test
    public void mainTestOK() {
        reservationRequest.setFullName(FULL_NAME);
        reservationRequest.setEmail(EMAIL);
        reservationRequest.setArrivalDate(ARRIVAL_DATE);
        reservationRequest.setDepartureDate(DEPARTURE_DATE);

        assertEquals(FULL_NAME, reservationRequest.getFullName());
        assertEquals(EMAIL, reservationRequest.getEmail());
        assertEquals(ARRIVAL_DATE, reservationRequest.getArrivalDate());
        assertEquals(DEPARTURE_DATE, reservationRequest.getDepartureDate());
    }
}