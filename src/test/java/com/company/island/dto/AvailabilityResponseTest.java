/*
 * Copyright (c) 2019.
 * Javier Azcurra - jazcurra.it@gmail.com
 */

package com.company.island.dto;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * @author Javier Azcurra
 * <p>
 * Test of {@link AvailabilityResponse}
 */

@RunWith(SpringRunner.class)
public class AvailabilityResponseTest {

    private static Date START_DATE1 = new Date(1234);
    private static Date END_DATE1 = new Date(4321);

    private static Date START_DATE2 = new Date(5667);
    private static Date END_DATE2 = new Date(7654);

    private AvailabilityResponse availabilityResponse;


    @Before
    public void setUp() throws Exception {
        availabilityResponse = new AvailabilityResponse(START_DATE1, END_DATE1);
    }

    @Test
    public void mainTestOK() {
        assertEquals(START_DATE1, availabilityResponse.getStartDate());
        assertEquals(END_DATE1, availabilityResponse.getEndDate());

        availabilityResponse.setStartDate(START_DATE2);
        availabilityResponse.setEndDate(END_DATE2);

        assertEquals(START_DATE2, availabilityResponse.getStartDate());
        assertEquals(END_DATE2, availabilityResponse.getEndDate());
    }

}