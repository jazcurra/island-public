/*
 * Copyright (c) 2019.
 * Javier Azcurra - jazcurra.it@gmail.com
 */

package com.company.island.repository;

import com.company.island.entity.UserEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

/**
 * @author Javier Azcurra
 * <p>
 * Test of {@link UserRepository}
 */

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void testFindByEmailOK() {
        UserEntity userEntity = userRepository.findByEmail("star@shield.com");
        assertNotNull(userEntity);
        assertEquals("Steve Rogers", userEntity.getFullName());
        assertEquals("star@shield.com", userEntity.getEmail());
    }

    @Test
    public void testFindByEmailFail() {
        UserEntity userEntity = userRepository.findByEmail("invalid@mail.com");
        assertNull(userEntity);
    }
}