/*
 * Copyright (c) 2019.
 * Javier Azcurra - jazcurra.it@gmail.com
 */

package com.company.island.repository;

import com.company.island.entity.ReservationEntity;
import org.apache.logging.log4j.util.Strings;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Javier Azcurra
 * <p>
 * Test of {@link ReservationRepository}
 */

@RunWith(SpringRunner.class)
@DataJpaTest
public class ReservationRepositoryTest {

    @Autowired
    private ReservationRepository reservationRepository;

    @Test
    public void testFindByBookingIdOK() {
        ReservationEntity reservationEntity = reservationRepository.findByBookingId("b1");
        assertNotNull(reservationEntity);
        assertEquals("b1", reservationEntity.getBookingId());
        assertEquals(new Long(1L), reservationEntity.getId());
        assertNotNull(reservationEntity.getUser());
        assertEquals(new Long(1L), reservationEntity.getUser().getId());
        assertEquals("Steve Rogers", reservationEntity.getUser().getFullName());
        assertEquals("star@shield.com", reservationEntity.getUser().getEmail());
    }

    @Test
    public void testFindByBookingIdFail() {
        ReservationEntity reservationEntity = reservationRepository.findByBookingId("idInvalid");
        assertNull(reservationEntity);

        reservationEntity = reservationRepository.findByBookingId(null);
        assertNull(reservationEntity);
    }

    @Test
    public void testFindAllByUserOK() {
        List<ReservationEntity> reservationEntities = reservationRepository.findAllByUserId(2);
        assertFalse(reservationEntities.isEmpty());
        assertEquals("b2", reservationEntities.get(0).getBookingId());
        assertEquals("b3", reservationEntities.get(1).getBookingId());
        assertEquals("b4", reservationEntities.get(2).getBookingId());
    }

    @Test
    public void testFindAllByUserFail() {
        List<ReservationEntity> reservationEntities = reservationRepository.findAllByUserId(20);
        assertTrue(reservationEntities.isEmpty());
    }

    @Test
    public void testFindByBookingIdAndUserEmailOK() {
        List<ReservationEntity> reservationEntities = reservationRepository.findByUserFullNameStartingWithIgnoreCaseAndUserEmailStartingWith("bRu", "bat");
        assertFalse(reservationEntities.isEmpty());
        assertEquals("b2", reservationEntities.get(0).getBookingId());
        assertEquals("b3", reservationEntities.get(1).getBookingId());
        assertEquals("b4", reservationEntities.get(2).getBookingId());
    }

    @Test
    public void testFindByBookingIdAndUserEmailOK2() {
        List<ReservationEntity> reservationEntities = reservationRepository.findByUserFullNameStartingWithIgnoreCaseAndUserEmailStartingWith(Strings.EMPTY, Strings.EMPTY);
        assertFalse(reservationEntities.isEmpty());
        assertEquals("b1", reservationEntities.get(0).getBookingId());
        assertEquals("b2", reservationEntities.get(1).getBookingId());
        assertEquals("b3", reservationEntities.get(2).getBookingId());
        assertEquals("b4", reservationEntities.get(3).getBookingId());
    }

    @Test
    public void testFindByBookingIdAndUserEmailFail() {
        List<ReservationEntity> reservationEntities = reservationRepository.findByUserFullNameStartingWithIgnoreCaseAndUserEmailStartingWith("c", "bat");
        assertTrue(reservationEntities.isEmpty());
    }

    @Test(expected = InvalidDataAccessApiUsageException.class)
    public void testFindByBookingIdAndUserEmailFail2() {
        reservationRepository.findByUserFullNameStartingWithIgnoreCaseAndUserEmailStartingWith(null, null);
        fail("InvalidDataAccessApiUsageException must be thrown!");
    }
}