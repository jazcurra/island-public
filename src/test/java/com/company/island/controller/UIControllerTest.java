/*
 * Copyright (c) 2019.
 * Javier Azcurra - jazcurra.it@gmail.com
 */

package com.company.island.controller;

import com.company.island.entity.ReservationEntity;
import com.company.island.mapper.ReservationMapper;
import com.company.island.service.ReservationService;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.company.island.dto.ReservationRequest;
import com.company.island.entity.UserEntity;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.validation.BindingResult;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.Collections;
import java.util.Date;

import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

/**
 * @author Javier Azcurra
 * <p>
 * Test of {@link UIController}
 */

@RunWith(SpringRunner.class)
@ComponentScan("com.company.island")
@WebMvcTest(UIController.class)
public class UIControllerTest {

    private static String URL_BASE = "/ui/reservations";

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ReservationService reservationService;

    @Autowired
    private ReservationMapper reservationMapper;

    private ReservationEntity reservationEntity;

    @Before
    public void setUp() throws Exception {
        reservationEntity = ReservationEntity.getBuilder()
                .withBookingId("b3")
                .withArrivalDate(new Date())
                .withDepartureDate(new Date())
                .isCancelled(Boolean.FALSE)
                .withUser(new UserEntity("test", "test3@mail.com")).build();
    }

    private static byte[] convertObjectToJsonBytes(Object object) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper.writeValueAsBytes(object);
    }

    @Test
    public void testList() throws Exception {
        when(reservationService.searchReservation(null, null)).thenReturn(Collections.singletonList(reservationEntity));
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post(URL_BASE + "/list")
                .contentType(MediaType.APPLICATION_JSON_UTF8);

        mvc.perform(builder)
                .andExpect(model().attributeExists("reservationList"))
                .andExpect(view().name("/main"));
    }

    @Test
    public void testDisplay() throws Exception {
        when(reservationService.getReservation(eq("b3"), eq(Boolean.TRUE))).thenReturn(reservationEntity);
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post(URL_BASE + "/display")
                .param("booking_id", "b3")
                .param("action", "edit")
                .contentType(MediaType.APPLICATION_JSON_UTF8);

        mvc.perform(builder)
                .andExpect(model().attribute("bookingId", equalTo("b3")))
                .andExpect(model().attributeExists("reservationRequest"))
                .andExpect(view().name("/res/edit"));

    }

    @Test
    public void testDisplay2() throws Exception {
        when(reservationService.getReservation(eq("b3"), eq(Boolean.TRUE))).thenReturn(reservationEntity);
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post(URL_BASE + "/display")
                .param("booking_id", "b3")
                .param("action", "cancel")
                .contentType(MediaType.APPLICATION_JSON_UTF8);

        mvc.perform(builder)
                .andExpect(model().attribute("bookingId", equalTo("b3")))
                .andExpect(model().attributeExists("reservationResponse"))
                .andExpect(view().name("/res/cancel"));
    }

    @Test
    public void testDisplayFail() throws Exception {
        when(reservationService.getReservation(eq("b3"), eq(Boolean.TRUE))).thenThrow(ResponseStatusException.class);
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post(URL_BASE + "/display")
                .param("booking_id", "b3")
                .param("action", "edit")
                .contentType(MediaType.APPLICATION_JSON_UTF8);

        mvc.perform(builder)
                .andExpect(model().attribute("bookingId", equalTo("b3")))
                .andExpect(view().name("/res/edit"));
    }

    @Test
    public void testShow() throws Exception {
        mvc.perform(post(URL_BASE + "/show"))
                .andExpect(view().name("/res/show"));
    }

    @Test
    public void testEdit() throws Exception {
        mvc.perform(post(URL_BASE + "/edit"))
                .andExpect(view().name("/res/edit"));
    }

    @Test
    public void testCancel() throws Exception {
        mvc.perform(post(URL_BASE + "/cancel"))
                .andExpect(view().name("/res/cancel"));
    }

    @Test
    public void testCreate() throws Exception {
        mvc.perform(post(URL_BASE + "/create"))
                .andExpect(model().attributeExists("reservationRequest"))
                .andExpect(view().name("/res/create"));
    }

    @Test
    public void testRevoke() throws Exception {
        when(reservationService.cancelReservation(eq("b3"), eq(Boolean.TRUE))).thenReturn(reservationEntity);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post(URL_BASE + "/revoke")
                .param("booking_id", "b3")
                .contentType(MediaType.APPLICATION_JSON_UTF8);

        mvc.perform(builder)
                .andExpect(model().attribute("successMsg", equalTo("label.reservation.cancelled.successfully")))
                .andExpect(model().attribute("args", equalTo("b3")))
                .andExpect(view().name("/res/cancel"));

    }

    @Test
    public void testRevokeFail() throws Exception {
        when(reservationService.cancelReservation(eq("b3"), eq(Boolean.TRUE))).thenThrow(ResponseStatusException.class);
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post(URL_BASE + "/revoke")
                .param("booking_id", "b3")
                .contentType(MediaType.APPLICATION_JSON_UTF8);
        mvc.perform(builder)
                .andExpect(model().attribute("bookingId", equalTo("b3")))
                .andExpect(view().name("/res/cancel"));
    }

    @Test
    public void testUpdate() throws Exception {
        ReservationRequest reservationRequest = reservationMapper.convertRequest(reservationEntity);
        when(reservationService.updateReservation(eq("b3"), any(ReservationRequest.class), any(BindingResult.class))).thenReturn(reservationEntity);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post(URL_BASE + "/update")
                .param("booking_id", "b3")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(convertObjectToJsonBytes(reservationRequest));
        mvc.perform(builder)
                .andExpect(model().attribute("successMsg", equalTo("label.reservation.edited.successfully")))
                .andExpect(model().attribute("args", equalTo(reservationEntity.getBookingId())))
                .andExpect(model().attributeDoesNotExist("reservationRequest"))
                .andExpect(view().name("/res/edit"));

    }

    @Test
    public void testUpdateFail() throws Exception {
        when(reservationService.updateReservation(eq("b3"), any(ReservationRequest.class), any(BindingResult.class))).thenThrow(ResponseStatusException.class);

        MockHttpServletRequestBuilder builder = post(URL_BASE + "/update")
                .param("booking_id", "b3")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(convertObjectToJsonBytes(new ReservationRequest()));

        mvc.perform(builder)
                .andExpect(model().attribute("bookingId", equalTo("b3")))
                .andExpect(model().attributeExists("reservationRequest"))
                .andExpect(view().name("/res/edit"));
    }

    @Test
    public void testSave() throws Exception {
        ReservationRequest reservationRequest = reservationMapper.convertRequest(reservationEntity);

        when(reservationService.createReservation(any(ReservationRequest.class), any(BindingResult.class))).thenReturn(reservationEntity);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post(URL_BASE + "/save")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(convertObjectToJsonBytes(reservationRequest));

        mvc.perform(builder)
                .andExpect(model().attribute("successMsg", equalTo("label.reservation.created.successfully")))
                .andExpect(model().attribute("args", equalTo(reservationEntity.getBookingId())))
                .andExpect(model().attributeExists("reservationRequest"))
                .andExpect(view().name("/res/create"));
    }

    @Test
    public void testSaveError() throws Exception {
        when(reservationService.createReservation(any(ReservationRequest.class), any(BindingResult.class))).thenThrow(ResponseStatusException.class);

        MockHttpServletRequestBuilder builder = post(URL_BASE + "/save")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(convertObjectToJsonBytes(new ReservationRequest()));

        mvc.perform(builder)
                .andExpect(view().name("/res/create"));
    }
}