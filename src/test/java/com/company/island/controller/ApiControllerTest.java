/*
 * Copyright (c) 2019.
 * Javier Azcurra - jazcurra.it@gmail.com
 */

package com.company.island.controller;

import com.company.island.entity.ReservationEntity;
import com.company.island.entity.UserEntity;
import com.company.island.mapper.ReservationMapper;
import com.company.island.service.ReservationService;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.company.island.dto.AvailabilityResponse;
import com.company.island.dto.CancelReservationRequest;
import com.company.island.dto.ReservationRequest;
import com.company.island.dto.ReservationResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.validation.BindingResult;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * @author Javier Azcurra
 * <p>
 * Test of {@link ApiController}
 */

@RunWith(SpringRunner.class)
@ComponentScan("com.company.island")
@WebMvcTest(ApiController.class)
public class ApiControllerTest {

    private static String URL_BASE = "/api/reservations";

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ReservationService reservationService;

    @Autowired
    private ReservationMapper reservationMapper;

    private static Date _ArrivalDate = new Date(1553764880L);
    private static Date _DepartureDate = new Date(1553851280L);

    private static byte[] convertObjectToJsonBytes(Object object) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper.writeValueAsBytes(object);
    }

    private static String parseDate(Date date) {
        final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return formatter.format(date);
    }

    @Test
    public void testGetReservationOK() throws Exception {

        ReservationEntity reservationEntity = ReservationEntity.getBuilder()
                .withBookingId("b1")
                .withArrivalDate(_ArrivalDate)
                .withDepartureDate(_DepartureDate)
                .isCancelled(Boolean.FALSE)
                .withUser(new UserEntity("test", "test@mail.com")).build();

        when(reservationService.getReservation("b1")).thenReturn(reservationEntity);
        ReservationResponse reservationResponse = reservationMapper.convertResponse(reservationEntity);

        mvc.perform(get(URL_BASE + "/b1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.bookingId", is(reservationResponse.getBookingId())))
                .andExpect(jsonPath("$.fullName", is(reservationResponse.getFullName())))
                .andExpect(jsonPath("$.email", is(reservationResponse.getEmail())))
                .andExpect(jsonPath("$.arrivalDate", is(parseDate(reservationResponse.getArrivalDate()))))
                .andExpect(jsonPath("$.departureDate", is(parseDate(reservationResponse.getDepartureDate()))));
    }

    @Test
    public void testGetListReservationsOK() throws Exception {

        ReservationEntity reservationEntity1 = ReservationEntity.getBuilder()
                .withBookingId("b1")
                .withArrivalDate(_ArrivalDate)
                .withDepartureDate(_DepartureDate)
                .isCancelled(Boolean.FALSE)
                .withUser(new UserEntity("test name", "test@mail.com")).build();

        ReservationEntity reservationEntity2 = ReservationEntity.getBuilder()
                .withBookingId("b2")
                .withArrivalDate(_ArrivalDate)
                .withDepartureDate(_DepartureDate)
                .isCancelled(Boolean.FALSE)
                .withUser(new UserEntity("test2 name", "test2@mail.com")).build();

        List<ReservationEntity> reservationEntityList = Arrays.asList(reservationEntity1, reservationEntity2);

        when(reservationService.searchReservation("t", "test")).thenReturn(reservationEntityList);
        List<ReservationResponse> reservationResponseList = reservationMapper.convertListResponse(reservationEntityList);

        mvc.perform(get(URL_BASE + "?full_name=t&email=test"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$[0].bookingId", is(reservationResponseList.get(0).getBookingId())))
                .andExpect(jsonPath("$[0].fullName", is(reservationResponseList.get(0).getFullName())))
                .andExpect(jsonPath("$[0].email", is(reservationResponseList.get(0).getEmail())))
                .andExpect(jsonPath("$[0].arrivalDate", is(parseDate(reservationResponseList.get(0).getArrivalDate()))))
                .andExpect(jsonPath("$[0].departureDate", is(parseDate(reservationResponseList.get(0).getDepartureDate()))))

                .andExpect(jsonPath("$[1].bookingId", is(reservationResponseList.get(1).getBookingId())))
                .andExpect(jsonPath("$[1].fullName", is(reservationResponseList.get(1).getFullName())))
                .andExpect(jsonPath("$[1].email", is(reservationResponseList.get(1).getEmail())))
                .andExpect(jsonPath("$[1].arrivalDate", is(parseDate(reservationResponseList.get(1).getArrivalDate()))))
                .andExpect(jsonPath("$[1].departureDate", is(parseDate(reservationResponseList.get(1).getDepartureDate()))));
    }

    @Test
    public void testGetAvailabilityDateRange() throws Exception {
        final Date startDate = _ArrivalDate, endDate = _DepartureDate;
        when(reservationService.getAvailabilityDateRange(null, null)).thenReturn(new AvailabilityResponse(startDate, endDate));
        mvc.perform(get(URL_BASE + "/availability"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.startDate", is(parseDate(startDate))))
                .andExpect(jsonPath("$.endDate", is(parseDate(endDate))));
    }

    @Test
    public void testCancelReservation() throws Exception {
        ReservationEntity reservationEntity = ReservationEntity.getBuilder()
                .withBookingId("b3")
                .withArrivalDate(_ArrivalDate)
                .withDepartureDate(_DepartureDate)
                .isCancelled(Boolean.TRUE)
                .withUser(new UserEntity("test", "test3@mail.com")).build();
        CancelReservationRequest cancelReservationRequest = new CancelReservationRequest();
        cancelReservationRequest.setCancel(Boolean.TRUE);
        ReservationResponse reservationResponse = reservationMapper.convertResponse(reservationEntity);

        when(reservationService.cancelReservation(eq("b3"), eq(Boolean.TRUE))).thenReturn(reservationEntity);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.patch(URL_BASE + "/b3")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(convertObjectToJsonBytes(cancelReservationRequest));

        mvc.perform(builder)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.bookingId", is(reservationResponse.getBookingId())))
                .andExpect(jsonPath("$.fullName", is(reservationResponse.getFullName())))
                .andExpect(jsonPath("$.email", is(reservationResponse.getEmail())))
                .andExpect(jsonPath("$.arrivalDate", is(parseDate(reservationResponse.getArrivalDate()))))
                .andExpect(jsonPath("$.departureDate", is(parseDate(reservationResponse.getDepartureDate()))))
                .andExpect(jsonPath("$.status", is(reservationResponse.getStatus())));
    }

    @Test
    public void testUpdateReservation() throws Exception {
        ReservationEntity reservationEntity = ReservationEntity.getBuilder()
                .withBookingId("b2")
                .withArrivalDate(_ArrivalDate)
                .withDepartureDate(_DepartureDate)
                .isCancelled(Boolean.FALSE)
                .withUser(new UserEntity("test", "test2@mail.com")).build();
        ReservationRequest reservationRequest = reservationMapper.convertRequest(reservationEntity);
        ReservationResponse reservationResponse = reservationMapper.convertResponse(reservationEntity);

        when(reservationService.updateReservation(eq("b2"), any(ReservationRequest.class), any(BindingResult.class))).thenReturn(reservationEntity);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.put(URL_BASE + "/b2")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(convertObjectToJsonBytes(reservationRequest));

        mvc.perform(builder)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.bookingId", is(reservationResponse.getBookingId())))
                .andExpect(jsonPath("$.fullName", is(reservationResponse.getFullName())))
                .andExpect(jsonPath("$.email", is(reservationResponse.getEmail())))
                .andExpect(jsonPath("$.arrivalDate", is(parseDate(reservationResponse.getArrivalDate()))))
                .andExpect(jsonPath("$.departureDate", is(parseDate(reservationResponse.getDepartureDate()))));
    }

    @Test
    public void testCreateReservation() throws Exception {
        ReservationEntity reservationEntity = ReservationEntity.getBuilder()
                .withBookingId("b3")
                .withArrivalDate(_ArrivalDate)
                .withDepartureDate(_DepartureDate)
                .isCancelled(Boolean.FALSE)
                .withUser(new UserEntity("test", "test3@mail.com")).build();
        ReservationRequest reservationRequest = reservationMapper.convertRequest(reservationEntity);
        ReservationResponse reservationResponse = reservationMapper.convertResponse(reservationEntity);

        when(reservationService.createReservation(any(ReservationRequest.class), any(BindingResult.class))).thenReturn(reservationEntity);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post(URL_BASE)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(convertObjectToJsonBytes(reservationRequest));

        mvc.perform(builder)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.bookingId", is(reservationResponse.getBookingId())))
                .andExpect(jsonPath("$.fullName", is(reservationResponse.getFullName())))
                .andExpect(jsonPath("$.email", is(reservationResponse.getEmail())))
                .andExpect(jsonPath("$.arrivalDate", is(parseDate(reservationResponse.getArrivalDate()))))
                .andExpect(jsonPath("$.departureDate", is(parseDate(reservationResponse.getDepartureDate()))));
    }

    @Test
    public void testDeleteReservation() throws Exception {
        mvc.perform(delete(URL_BASE + "/b1"))
                .andExpect(status().isNoContent());
    }
}