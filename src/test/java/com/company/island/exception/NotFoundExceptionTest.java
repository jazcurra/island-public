/*
 * Copyright (c) 2019.
 * Javier Azcurra - jazcurra.it@gmail.com
 */

package com.company.island.exception;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.mock;

/**
 * @author Javier Azcurra
 * <p>
 * Test of {@link NotFoundException}
 */

@RunWith(SpringRunner.class)
public class NotFoundExceptionTest {
    @Test(expected = NotFoundException.class)
    public void simpleTest() {
        throw new NotFoundException();
    }

    @Test(expected = NotFoundException.class)
    public void simpleTest2() {
        throw new NotFoundException("anyReason");
    }

    @Test(expected = NotFoundException.class)
    public void simpleTest3() {
        throw new NotFoundException("anyReason", mock(Throwable.class));
    }
}