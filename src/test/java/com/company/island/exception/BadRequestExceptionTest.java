/*
 * Copyright (c) 2019.
 * Javier Azcurra - jazcurra.it@gmail.com
 */

package com.company.island.exception;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.mock;

/**
 * @author Javier Azcurra
 * <p>
 * Test of {@link BadRequestException}
 */

@RunWith(SpringRunner.class)
public class BadRequestExceptionTest {

    @Test(expected = BadRequestException.class)
    public void simpleTest() {
        throw new BadRequestException();
    }

    @Test(expected = BadRequestException.class)
    public void simpleTest2() {
        throw new BadRequestException("anyReason");
    }

    @Test(expected = BadRequestException.class)
    public void simpleTest3() {
        throw new BadRequestException("anyReason", mock(Throwable.class));
    }
}