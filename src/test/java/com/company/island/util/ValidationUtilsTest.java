/*
 * Copyright (c) 2019.
 * Javier Azcurra - jazcurra.it@gmail.com
 */

package com.company.island.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

/**
 * @author Javier Azcurra
 * <p>
 * Test of {@link ValidationUtils}
 */

@RunWith(SpringRunner.class)
public class ValidationUtilsTest {

    @Test
    public void testEmailOK() {
        assertTrue(ValidationUtils.validateEmailFormat("testOK@mail.com "));
        assertTrue(ValidationUtils.validateEmailFormat(" testOK@mail.com "));
        assertTrue(ValidationUtils.validateEmailFormat("    testOK@mail.com"));
        assertTrue(ValidationUtils.validateEmailFormat("testOK@mail.com     "));
    }

    @Test
    public void testEmailFail() {
        assertFalse(ValidationUtils.validateEmailFormat(null));
        assertFalse(ValidationUtils.validateEmailFormat(" testmail.com "));
        assertFalse(ValidationUtils.validateEmailFormat("testmail.com"));
        assertFalse(ValidationUtils.validateEmailFormat(""));
        assertFalse(ValidationUtils.validateEmailFormat(" "));
    }

    @Test
    public void testNormalizeString() {
        assertEquals("testok@mail.com", ValidationUtils.normalizeString("testOK@mail.com "));
        assertEquals("testok@mail.com", ValidationUtils.normalizeString(" TestOK@mail.com "));
        assertEquals("testok@mail.com", ValidationUtils.normalizeString("    tEstOK@mail.com"));
        assertEquals("testok@mail.com", ValidationUtils.normalizeString("testOK@Mail.com     "));
        assertEquals("", ValidationUtils.normalizeString(""));
        assertNull(ValidationUtils.normalizeString(null));
    }

    @Test
    public void testWrapArray() {
        assertArrayEquals(new Object[]{"testOK"}, ValidationUtils.wrapArray("testOK"));
        assertArrayEquals(new Object[]{"testOK", 2}, ValidationUtils.wrapArray("testOK", 2));
        assertArrayEquals(new Object[]{"testOK", 2, true}, ValidationUtils.wrapArray("testOK", 2, true));
        assertArrayEquals(new Object[]{}, ValidationUtils.wrapArray());
        assertArrayEquals(new Object[]{"", " "}, ValidationUtils.wrapArray("", " "));
    }
}