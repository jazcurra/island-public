/*
 * Copyright (c) 2019.
 * Javier Azcurra - jazcurra.it@gmail.com
 */

package com.company.island.entity.enumeration;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author Javier Azcurra
 * <p>
 * Test of {@link ReservationStatus}
 */

@RunWith(SpringRunner.class)
public class ReservationStatusTest {

    @Test
    public void mainTest() {
        Assert.assertEquals(
                ReservationStatus.ACTIVE.status,
                ReservationStatus.ACTIVE.toString());

        Assert.assertEquals(
                ReservationStatus.CANCELLED.status,
                ReservationStatus.CANCELLED.toString());

        Assert.assertEquals(
                ReservationStatus.EXPIRED.status,
                ReservationStatus.EXPIRED.toString());

        Assert.assertTrue(ReservationStatus.ACTIVE.equals(ReservationStatus.ACTIVE));
        Assert.assertFalse(ReservationStatus.ACTIVE.equals(ReservationStatus.CANCELLED));
    }
}