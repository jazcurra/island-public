/*
 * Copyright (c) 2019.
 * Javier Azcurra - jazcurra.it@gmail.com
 */

package com.company.island.entity;


import com.company.island.entity.enumeration.ReservationStatus;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import static org.junit.Assert.*;

/**
 * @author Javier Azcurra
 * <p>
 * Test of {@link ReservationEntity}
 */

@RunWith(SpringRunner.class)
public class ReservationEntityTest {

    private static String _BookingId1 = "bid1";
    private static String _BookingId2 = "bid2";
    private static Date _ArrivalDate1 = new Date(new Random().nextInt());
    private static Date _ArrivalDate2 = new Date();
    private static Date _DepartureDate1 = new Date(new Random().nextInt());
    private static Date _CreationDate1 = new Date(new Random().nextInt());
    private static Date _UpdateDate1 = new Date(new Random().nextInt());
    private static Date _CancelDate1 = new Date(new Random().nextInt());
    private static boolean _IsCancel1 = Boolean.TRUE;
    private final UserEntity user1 = new UserEntity("test" + new Random().nextInt(), "test" + new Random().nextInt() + "@mail.com");

    private ReservationEntity reservationEntity1;
    private ReservationEntity reservationEntity2;

    @Before
    public void setUp() throws Exception {
        reservationEntity1 = new ReservationEntity();
        reservationEntity1.setBookingId(_BookingId1);
        reservationEntity1.setUser(user1);
        reservationEntity1.setArrivalDate(_ArrivalDate1);
        reservationEntity1.setDepartureDate(_DepartureDate1);
        reservationEntity1.setCreationDate(_CreationDate1);
        reservationEntity1.setUpdateDate(_UpdateDate1);
        reservationEntity1.setCancelDate(_CancelDate1);
        reservationEntity1.setCancelled(_IsCancel1);

        reservationEntity2 = new ReservationEntity();
        reservationEntity2.setBookingId(_BookingId2);
        reservationEntity2.setUser(user1);
        reservationEntity2.setArrivalDate(_ArrivalDate2);
        reservationEntity2.setDepartureDate(_DepartureDate1);
        reservationEntity2.setCreationDate(_CreationDate1);
        reservationEntity2.setUpdateDate(_UpdateDate1);
        reservationEntity2.setCancelDate(_CancelDate1);
        reservationEntity2.setCancelled(!_IsCancel1);
    }

    @Test
    public void mainTest() {
        assertNotNull(reservationEntity1);
        assertEquals(_BookingId1, reservationEntity1.getBookingId());
        assertEquals(user1, reservationEntity1.getUser());
        assertEquals(_ArrivalDate1, reservationEntity1.getArrivalDate());
        assertEquals(_DepartureDate1, reservationEntity1.getDepartureDate());
        assertEquals(_CreationDate1, reservationEntity1.getCreationDate());
        assertEquals(_UpdateDate1, reservationEntity1.getUpdateDate());
        assertEquals(_CancelDate1, reservationEntity1.getCancelDate());
        assertEquals(_IsCancel1, reservationEntity1.isCancelled());
        Assert.assertEquals(ReservationStatus.CANCELLED, reservationEntity1.getReservationStatus());
        assertNull(reservationEntity1.getId());
    }

    @Test
    public void reservationEqualsTest() {
        assertTrue(reservationEntity1.equals(reservationEntity1));
        assertFalse(reservationEntity1.equals(reservationEntity2));
        assertEquals(reservationEntity1.hashCode(), reservationEntity1.hashCode());
        assertNotEquals(reservationEntity1.hashCode(), reservationEntity2.hashCode());
    }

    @Test
    public void reservationToStringTest() {
        assertNotNull(reservationEntity1.toString());
    }

    @Test
    public void reservationOnCreateTest() {
        reservationEntity1 = new ReservationEntity();
        reservationEntity1.onCreate();
        assertNotEquals(_BookingId1, reservationEntity1.getBookingId());
        assertNotNull(reservationEntity1.getCreationDate());
        assertFalse(reservationEntity1.isCancelled());
    }

    @Test
    public void reservationOnUpdateTest() {
        reservationEntity1 = new ReservationEntity();
        reservationEntity1.setCancelled(Boolean.TRUE);
        reservationEntity1.onUpdate();
        assertNotNull(reservationEntity1.getUpdateDate());
        assertNotNull(reservationEntity1.getCancelDate());
    }

    @Test
    public void testActiveReservation() {
        Date dt = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(dt);
        c.add(Calendar.DATE, 2);
        dt = c.getTime();

        reservationEntity2.setArrivalDate(dt);
        Assert.assertEquals(ReservationStatus.ACTIVE, reservationEntity2.getReservationStatus());
        assertEquals(ReservationStatus.ACTIVE.toString(), reservationEntity2.getReservationStatusValue());
    }

    @Test
    public void testCancelledReservation() {
        reservationEntity2.setCancelled(Boolean.TRUE);
        Assert.assertEquals(ReservationStatus.CANCELLED, reservationEntity2.getReservationStatus());
        assertEquals(ReservationStatus.CANCELLED.toString(), reservationEntity2.getReservationStatusValue());
    }


    @Test
    public void testExpiredReservation() {
        Date dt = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(dt);
        c.add(Calendar.DATE, -3);
        dt = c.getTime();

        reservationEntity2.setArrivalDate(dt);
        Assert.assertEquals(ReservationStatus.EXPIRED, reservationEntity2.getReservationStatus());
        assertEquals(ReservationStatus.EXPIRED.toString(), reservationEntity2.getReservationStatusValue());
    }
}