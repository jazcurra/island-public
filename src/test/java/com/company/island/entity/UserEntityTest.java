/*
 * Copyright (c) 2019.
 * Javier Azcurra - jazcurra.it@gmail.com
 */

package com.company.island.entity;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

/**
 * @author Javier Azcurra
 * <p>
 * Test of {@link UserEntity}
 */

@RunWith(SpringRunner.class)
public class UserEntityTest {

    private final static String FULLNAME1 = "user test1";
    private final static String FULLNAME2 = "user test2";
    private final static String EMAIL1 = "test1@mail.com";
    private final static String EMAIL2 = "test2@mail.com";

    private UserEntity user1;
    private UserEntity user2;

    @Before
    public void setUp() throws Exception {
        user1 = new UserEntity();
        user1.setFullName(FULLNAME1);
        user1.setEmail(EMAIL1);
        user2 = new UserEntity(FULLNAME2, EMAIL2);
    }

    @Test
    public void testUser() {
        assertEquals(FULLNAME1, user1.getFullName());
        assertEquals(EMAIL1, user1.getEmail());
        assertEquals(FULLNAME2, user2.getFullName());
        assertEquals(EMAIL2, user2.getEmail());
        assertNull(user1.getId());
    }

    @Test
    public void testEqualsUser() {
        assertTrue(user1.equals(user1));
        assertFalse(user1.equals(user2));

        assertEquals(user1.hashCode(), user1.hashCode());
        assertNotEquals(user1.hashCode(), user2.hashCode());
    }

    @Test
    public void testToString() {
        final String FULLNAME3 = "user test3";
        final String EMAIL3 = "test3@mail.com";
        user1.setFullName(FULLNAME3);
        user1.setEmail(EMAIL3);
        final String userToString = "UserEntity[fullName='" + user1.getFullName() + "', email='" + user1.getEmail() + "']";
        assertEquals(userToString, user1.toString());
    }
}