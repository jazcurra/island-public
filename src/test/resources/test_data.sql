-- Copyright (c) 2019. Author Javier Azcurra <jazcurra.it@gmail.com>

insert into `user` (`FULL_NAME`,`EMAIL`) values ('Steve Rogers', 'star@shield.com');
insert into `user` (`FULL_NAME`,`EMAIL`) values ('Bruce Wayne', 'bat@gotic.com');

insert into reservation (`BOOKING_ID`,`USER_ID`,`CANCELLED`, `ARRIVAL_DATE`,`DEPARTURE_DATE`, `CREATION_DATE`) values ('b1', 1, FALSE ,DATEADD('DAY',4, CURRENT_DATE), DATEADD('DAY',6, CURRENT_DATE), TO_DATE('01/01/2018', 'DD/MM/YYYY'));
insert into reservation (`BOOKING_ID`,`USER_ID`,`CANCELLED`, `ARRIVAL_DATE`,`DEPARTURE_DATE`, `CREATION_DATE`) values ('b2', 2, FALSE ,DATEADD('DAY',4, CURRENT_DATE), DATEADD('DAY',6, CURRENT_DATE), TO_DATE('01/01/2018', 'DD/MM/YYYY'));
insert into reservation (`BOOKING_ID`,`USER_ID`,`CANCELLED`, `ARRIVAL_DATE`,`DEPARTURE_DATE`, `CREATION_DATE`) values ('b3', 2, FALSE ,DATEADD('DAY',4, CURRENT_DATE), DATEADD('DAY',6, CURRENT_DATE), TO_DATE('01/01/2018', 'DD/MM/YYYY'));
insert into reservation (`BOOKING_ID`,`USER_ID`,`CANCELLED`, `ARRIVAL_DATE`,`DEPARTURE_DATE`, `CREATION_DATE`) values ('b4', 2, FALSE ,DATEADD('DAY',4, CURRENT_DATE), DATEADD('DAY',6, CURRENT_DATE), TO_DATE('01/01/2018', 'DD/MM/YYYY'));
