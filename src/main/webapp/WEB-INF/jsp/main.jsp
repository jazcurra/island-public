<%--
  ~ Copyright (c) 2019.
  ~ Javier Azcurra - jazcurra.it@gmail.com
  --%>

<%@ taglib prefix="string" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper>
    <jsp:attribute name="content">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        <string:message code="label.list.reservation"/>
                    </h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <jsp:include page="res/_message.jsp"/>
                <jsp:include page="res/_list.jsp"/>
            </div>
        </div>
    </jsp:attribute>
</t:wrapper>