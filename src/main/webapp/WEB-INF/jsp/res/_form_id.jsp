<%--
~ Copyright (c) 2019.
~ Javier Azcurra - jazcurra.it@gmail.com
  --%>

<%@ taglib prefix="string" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script type="text/javascript">
    function setBookingId() {
        let trimValue = $.trim($("#bookingId").val());
        $("#booking_id").val(trimValue);
    }
</script>

<c:set var="urlBase" value="${pageContext.request.contextPath}/ui/reservations"/>
<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <string:message code="label.searchby.bookingid"/>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">

                        <form role="form" action="${urlBase}/display" onsubmit="setBookingId()">
                            <input type="hidden" id="action" name="action" value="${param.action}">
                            <input type="hidden" id="booking_id" name="booking_id" value="">
                            <div class="form-group input-group">
                                <input class="form-control" id="bookingId" value="${bookingId}" maxlength="90"
                                       required="required" placeholder="<string:message code="label.reservation.bookingId"/>"/>

                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

