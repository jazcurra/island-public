<%--
~ Copyright (c) 2019.
~ Javier Azcurra - jazcurra.it@gmail.com
  --%>

<%@ taglib prefix="string" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script type="text/javascript">
    function setValues() {
        let trimValue = $.trim($("#fullName").val());
        $("#full_name").val(trimValue);

        trimValue = $.trim($("#email-input").val());
        $("#email").val(trimValue);
    }

    function cleanValues() {
        $("#full_name").val('');
        $("#email").val('');
    }
</script>

<c:set var="urlBase" value="${pageContext.request.contextPath}/ui/reservations"/>
<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <string:message code="label.searchby.fullname.Email"/>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-10">

                        <form role="form" action="${urlBase}/list">
                            <input type="hidden" id="full_name" name="full_name" value="">
                            <input type="hidden" id="email" name="email" value="">
                            <div class="form-group form-inline">
                                <input class="form-control" id="fullName" value="${fullName}" maxlength="90"
                                       placeholder="<string:message code="label.reservation.fullname"/>"/>
                                <input class="form-control" id="email-input" value="${email}" maxlength="90"
                                       placeholder="<string:message code="label.reservation.email"/>"/>

                                <button type="submit" class="btn btn-success" onclick="setValues()"><string:message
                                        code="label.button.search" /></button>
                                <button type="submit" class="btn btn-info" onclick="cleanValues()"><string:message
                                        code="label.button.form.clean"/></button>

                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

