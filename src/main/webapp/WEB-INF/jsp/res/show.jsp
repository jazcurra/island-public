<%--
~ Copyright (c) 2019.
~ Javier Azcurra - jazcurra.it@gmail.com
  --%>

<%@ taglib prefix="string" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:wrapper>
    <jsp:attribute name="content">

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><string:message code="label.show"/></h1>
                </div>
            </div>

            <jsp:include page="_form_id.jsp"/>
            <c:if test="${reservationResponse != null}">
                <jsp:include page="_display_info.jsp">
                    <jsp:param name="op" value="show"/>
                </jsp:include>
            </c:if>
        </div>
    </jsp:attribute>
</t:wrapper>