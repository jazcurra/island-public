<%--
~ Copyright (c) 2019.
~ Javier Azcurra - jazcurra.it@gmail.com
  --%>

<%@ taglib prefix="string" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script type="application/javascript">
    $(function () {
        $.ajax({
            type: 'GET',
            url: "/island/api/reservations/availability?date_type=arrival",
            success: function (result) {
                $("#arr-date").attr("min", result.startDate).attr("max", result.endDate);
            }
        });

        $("#reservationRequest").submit(function (e) {
            let arrival = $("#arr-date").val();
            let departure = $("#dep-date").val();
            let askConfirmation = confirm('Confirm reservation from [' + arrival + '] to [' + departure + '] ?');
            if (askConfirmation === false) {
                e.preventDefault(e);
            }
        });
    });

    function activateDep(startDate) {
        if (startDate === '') {
            $("#dep-date").val("").attr("readonly", true);
            $("#submit-btn").attr('disabled', true);
        } else {
            $.ajax({
                type: 'GET',
                url: "/island/api/reservations/availability?date_type=departure&arrival_date=" + encodeURIComponent(startDate),
                success: function (result) {
                    $("#dep-date").attr("readonly", false).val(result.endDate).attr("min", result.startDate).attr("max", result.endDate);
                    $("#submit-btn").removeAttr('disabled');
                }
            });
        }
    }

    function trimFormValue() {
        let trimValue = $.trim($("#fullName").val());
        $("#fullName").val(trimValue);

        trimValue = $.trim($("#email").val());
        $("#email").val(trimValue);
    }
</script>

<c:set var="urlBase" value="${pageContext.request.contextPath}/ui/reservations"/>
<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <string:message code="label.reservation.form"/>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">

                        <form:form method="POST" action="${urlBase}/${param.op}" modelAttribute="reservationRequest" onsubmit="trimFormValue()">
                            <input type="hidden" id="booking_id" name="booking_id" value="${bookingId}">
                            <div class="form-group">
                                <label><string:message code="label.reservation.fullname"/></label>
                                <form:input class="form-control read-on" path="fullName" required="required"
                                            maxlength="90" readonly="${param.op == 'update' ? true : false}"/>
                            </div>

                            <div class="form-group">
                                <label><string:message code="label.reservation.email"/></label>
                                <form:input class="form-control read-on" path="email" required="required" type="email"
                                            maxlength="90" readonly="${param.op == 'update' ? true : false}"/>
                            </div>

                            <div class="form-group">
                                <label><string:message code="label.reservation.arrivalDate"/></label>
                                <div class="input-group date" data-provide="datepicker">
                                    <form:input class="form-control" required="required" id="arr-date"
                                                path="arrivalDate"
                                                type="date" onchange="activateDep(this.value)"/>
                                    <div class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label><string:message code="label.reservation.departureDate"/></label>
                                <div class="input-group date" data-provide="datepicker">
                                    <form:input class="form-control" required="required" id="dep-date"
                                                path="departureDate"
                                                type="date" readonly="true"/>
                                    <div class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </div>
                                </div>
                            </div>

                            <button id="submit-btn" type="submit" class="btn btn-success"
                                    disabled="${param.op == 'update' ? true : false}"><string:message
                                    code="label.button.form.submit.reservation"/></button>
                            <button type="reset" class="btn btn-info" onclick="activateDep('')"><string:message
                                    code="label.button.form.clean"/></button>
                        </form:form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>