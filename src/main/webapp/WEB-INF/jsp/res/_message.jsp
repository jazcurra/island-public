<%--
~ Copyright (c) 2019.
~ Javier Azcurra - jazcurra.it@gmail.com
  --%>
<%@ taglib prefix="string" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${errorMsg != null}">
    <div class="alert alert-danger">
        <string:message code="label.validation.errors"/>
        <ul>
            <c:forEach items="${errorMsg.split(\",\")}" var="error">
                <li>
                    ${error}
                </li>
            </c:forEach>
        </ul>
    </div>
</c:if>
<c:if test="${successMsg != null}">
    <div class="alert alert-success">
        <string:message code="${successMsg}" arguments="${args}"/>
    </div>
</c:if>