<%--
~ Copyright (c) 2019.
~ Javier Azcurra - jazcurra.it@gmail.com
  --%>
<%@ taglib prefix="string" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script type="application/javascript">
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });

    function deleteReservation(id, msg) {
        let askConfirmation = confirm(msg);
        if (askConfirmation === false) {
            return;
        }

        $.ajax({
            type: 'DELETE',
            url: "/island/api/reservations/" + id,
            success: function (result) {
                location.reload();
            },
            error: function (result) {
                alert("Fail to Delete!")
            }
        });
    }

    function editReservation(url, id) {
        window.location = url + "/display?action=edit&booking_id=" + id;
    }

    function cancelReservation(url, id) {
        window.location = url + "/display?action=cancel&booking_id=" + id;
    }
</script>

<jsp:include page="_search_table.jsp"/>

<c:set var="urlBase" value="${pageContext.request.contextPath}/ui/reservations"/>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <string:message code="label.actual.list.reservation"/>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div style="overflow:auto; height: 350px;">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th><string:message code="label.reservation.bookingId"/></th>
                            <th><string:message code="label.reservation.fullname"/></th>
                            <th><string:message code="label.reservation.email"/></th>
                            <th><string:message code="label.reservation.arrivalDate"/></th>
                            <th><string:message code="label.reservation.departureDate"/></th>
                            <th><string:message code="label.reservation.status"/></th>
                            <th><string:message code="label.table.actions"/></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="entry" items="${reservationList}">
                            <tr>
                                <td>${entry.getBookingId()}
                                <td>${entry.getFullName()}
                                <td>${entry.getEmail()}
                                <td>${entry.getArrivalDate()}
                                <td>${entry.getDepartureDate()}
                                <td>${entry.getStatus()}
                                <td>
                                    <p>
                                        <button data-toggle="tooltip" data-placement="top" title="" data-original-title="<string:message code="label.edit"/>"
                                                style="${entry.getStatus() == 'Active' ? 'display: ;' : 'display: none;'}"
                                                type="button" class="btn btn-info btn-circle"
                                                onclick="editReservation('${urlBase}', '${entry.getBookingId()}')">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                        <button data-toggle="tooltip" data-placement="top" title="" data-original-title="<string:message code="label.cancel"/>"
                                                style="${entry.getStatus() == 'Active' ? 'display: ;' : 'display: none;'}"
                                                type="button" class="btn btn-warning btn-circle"
                                                onclick="cancelReservation('${urlBase}', '${entry.getBookingId()}')">
                                            <i class="fa fa-ban"></i>
                                        </button>
                                        <button data-toggle="tooltip" data-placement="top" title="" data-original-title="<string:message code="label.delete"/>"
                                                type="button" class="btn btn-danger btn-circle"
                                                onclick="deleteReservation('${entry.getBookingId()}','<string:message code="confirm.delete"/>')">
                                            <i class="fa fa-times"></i>
                                        </button>

                                    </p>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>