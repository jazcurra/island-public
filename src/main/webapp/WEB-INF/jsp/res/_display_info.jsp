<%--
~ Copyright (c) 2019.
~ Javier Azcurra - jazcurra.it@gmail.com
  --%>

<%@ taglib prefix="string" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script type="text/javascript">
    function trimDelValue() {
        let trimValue = $.trim($("#del_booking_id").val());
        $("#del_booking_id").val(trimValue);
    }
</script>

<c:set var="urlBase" value="${pageContext.request.contextPath}/ui/reservations"/>
<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-body">
                <h3><string:message code="label.reservation.details"/></h3>
                <p></p>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th><string:message code="label.reservation.fullname"/>
                            <td>${reservationResponse.getFullName()}
                        </tr>
                        <tr>
                            <th><string:message code="label.reservation.email"/>
                            <td>${reservationResponse.getEmail()}
                        </tr>
                        <tr>
                            <th><string:message code="label.reservation.arrivalDate"/>
                            <td>${reservationResponse.getArrivalDate()}
                        </tr>
                        <tr>
                            <th><string:message code="label.reservation.departureDate"/>
                            <td>${reservationResponse.getDepartureDate()}
                        </tr>
                        </tbody>
                    </table>
                </div>
                <c:if test="${param.op == \"delete\"}">
                    <p>
                    <form role="form" id="submit-cancel" action="${urlBase}/revoke"
                          onsubmit="trimDelValue(); return confirm('<string:message code="confirm.cancel"/>');">
                        <input type="hidden" id="del_booking_id" name="booking_id" value="${bookingId}">
                        <button type="submit" class="btn btn-danger">
                            <string:message code="label.button.form.cancel.reservation"/>
                        </button>
                    </form>
                    </p>
                </c:if>
            </div>
        </div>
    </div>
    <!-- /.col-lg-6 -->
</div>