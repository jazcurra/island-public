<%--
~ Copyright (c) 2019.
~ Javier Azcurra - jazcurra.it@gmail.com
  --%>

<%@ taglib prefix="string" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper>
    <jsp:attribute name="content">

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><string:message code="label.create"/></h1>
                </div>
            </div>

            <jsp:include page="_message.jsp"/>
            <jsp:include page="_form.jsp">
                <jsp:param name="op" value="save"/>
            </jsp:include>

        </div>

    </jsp:attribute>
</t:wrapper>