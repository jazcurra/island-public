<%--
~ Copyright (c) 2019.
~ Javier Azcurra - jazcurra.it@gmail.com
  --%>
<%@ tag language="java" pageEncoding="UTF-8" %>
<%@ attribute name="content" fragment="true" %>
<%@ taglib prefix="string" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><string:message code="label.title.island"/></title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="css/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sbadmin/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <script defer src="https://use.fontawesome.com/releases/v5.6.3/js/all.js"
            integrity="sha384-EIHISlAOj4zgYieurP0SdoiBYfGJKkgWedPHH4jCzpCXLmzVsw1ouK59MuUtP4a1"
            crossorigin="anonymous"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="js/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/sbadmin/sb-admin-2.js"></script>

    <c:set var="urlBase" value="${pageContext.request.contextPath}/ui/reservations"/>
</head>

<body>

<!-- Wrapper -->
<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-brand" href="${urlBase}/list">
                <string:message code="label.title.island"/> </a>
        </div>
        <!-- /.navbar-header -->

        <div class="navbar-default sidebar" role="navigation">
            <div class="">
                <ul class="nav">
                    <li>
                        <a href="#"><i class="fa fa-table fa-fw"></i>
                            <string:message code="label.list.reservation"/>
                        </a>
                        <ul class="nav nav-second-level collapse in">
                            <li>
                                <a href="${urlBase}/list">
                                    <string:message code="label.list"/>
                                </a>
                            </li>
                            <li>
                                <a href="${urlBase}/create">
                                    <string:message code="label.create"/>
                                </a>
                            </li>
                            <li>
                                <a href="${urlBase}/edit">
                                    <string:message code="label.edit"/>
                                </a>
                            </li>
                            <li>
                                <a href="${urlBase}/cancel">
                                    <string:message code="label.cancel"/>
                                </a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>

    <jsp:invoke fragment="content"/>

    <footer>
        <div style="position: fixed; left: 0; bottom: 0; width: 100%; text-align: right">
            <b><string:message code="label.copyright"/></b>
        </div>
    </footer>

</div>
<!-- /.Wrapper -->

</body>
</html>
