/*
 * Copyright (c) 2019.
 * Javier Azcurra - jazcurra.it@gmail.com
 */

package com.company.island.service;

import com.company.island.builder.ReservationBuilder;
import com.company.island.dto.AvailabilityResponse;
import com.company.island.dto.ReservationRequest;
import com.company.island.entity.ReservationEntity;
import com.company.island.entity.UserEntity;
import com.company.island.entity.enumeration.ReservationStatus;
import com.company.island.exception.NotFoundException;
import com.company.island.repository.ReservationRepository;
import com.company.island.repository.UserRepository;
import com.company.island.validator.ReservationValidator;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import java.util.Date;
import java.util.List;

/**
 * @author Javier Azcurra
 * <p>
 * Reservation Service that contains reservation business logic
 */

@Service
public class ReservationService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ReservationRepository reservationRepository;

    @Autowired
    private ReservationValidator reservationValidator;

    @Autowired
    private DateService dateService;

    /**
     * Check if reservation entity status is equals to reservationStatus enum input
     *
     * @param reservationEntity
     * @param reservationStatus
     * @return boolean - true if equals and false if is not
     */
    public boolean checkReservationStatus(final ReservationEntity reservationEntity, final ReservationStatus reservationStatus) {
        return reservationEntity.getReservationStatus().equals(reservationStatus);
    }

    /**
     * Get a specific reservation based on bookingId
     *
     * @param bookingId
     * @return a ReservationEntity
     * @throws NotFoundException if reservation is not found or is not active
     */
    public ReservationEntity getReservation(String bookingId, boolean isActive) {
        bookingId = (bookingId == null) ? Strings.EMPTY : bookingId.trim();
        ReservationEntity reservationEntity = reservationRepository.findByBookingId(bookingId);
        if (reservationEntity == null) {
            reservationValidator.notFoundResponse("reservation.NotFound", bookingId);
        }

        if (isActive && !checkReservationStatus(reservationEntity, ReservationStatus.ACTIVE)) {
            reservationValidator.notFoundResponse("reservation.NotFound", bookingId);
        }

        return reservationEntity;
    }

    /**
     * Get a specific reservation based on bookingId
     *
     * @param bookingId
     * @return a ReservationEntity
     * @throws NotFoundException if reservation is not found
     */
    public ReservationEntity getReservation(String bookingId) {
        return getReservation(bookingId, Boolean.FALSE);
    }

    /**
     * Get a list of reservations based on search [fullName, email] attributes
     *
     * @param fullName
     * @param email
     * @return a List of ReservationEntity
     */
    public List<ReservationEntity> searchReservation(String fullName, String email) {
        if (Strings.isBlank(fullName) && Strings.isBlank(email)) {
            return reservationRepository.findAll();
        }
        fullName = (fullName == null) ? Strings.EMPTY : fullName.trim();
        email = (email == null) ? Strings.EMPTY : email.trim().toLowerCase();

        return reservationRepository.findByUserFullNameStartingWithIgnoreCaseAndUserEmailStartingWith(fullName, email);
    }

    /**
     * Return the Dates ranges available for arrival Date and Departure Date
     *
     * @param dateType
     * @param arrivalDateSelected
     * @return AvailabilityResponse Dto
     */
    public AvailabilityResponse getAvailabilityDateRange(final String dateType, final Date arrivalDateSelected) {
        final Date start, end;

        if ("departure".equalsIgnoreCase(dateType)) {
            //departure date range
            if (arrivalDateSelected == null) {
                reservationValidator.badRequestResponse("arrivalDate.notEmpty");
            }

            if (!dateService.checkArrivalAfterMinTimeReserve(arrivalDateSelected)) {
                reservationValidator.badRequestResponse("arrivalDate.min.time.availability.daterange", dateService.getMinDateOfArrival());
            }

            if (!dateService.checkArrivalBeforeMaxTimeReserve(arrivalDateSelected)) {
                reservationValidator.badRequestResponse("arrivalDate.max.time.availability.daterange", dateService.getMaxDateOfArrival());
            }

            start = dateService.getMinDateDepartureOfArrival(arrivalDateSelected);
            end = dateService.getMaxDateDepartureOfArrival(arrivalDateSelected);
        } else {
            //default arrival date range
            start = dateService.getMinDateOfArrival();
            end = dateService.getMaxDateOfArrival();
        }

        return new AvailabilityResponse(start, end);
    }

    /**
     * Cancel a reservation based on bookingId
     * The reservation selected must be ACTIVE
     *
     * @param bookingId
     * @return a specific ReservationEntity cancelled
     */
    public ReservationEntity cancelReservation(final String bookingId, final Boolean cancelFlag) {
        final ReservationEntity reservationEntity = getReservation(bookingId, Boolean.TRUE);

        if (!Boolean.TRUE.equals(cancelFlag))
            reservationValidator.badRequestResponse("cancel.must.true");

        reservationEntity.setCancelled(cancelFlag);
        return reservationRepository.saveAndFlush(reservationEntity);
    }

    /**
     * Delete a reservation based on bookingId
     *
     * @param bookingId
     */
    public void deleteReservation(final String bookingId) {
        final ReservationEntity reservationEntity = getReservation(bookingId);
        reservationRepository.delete(reservationEntity);
    }

    /**
     * Update a reservation based on bookingId and ReservationRequest dto
     * The reservation selected must be ACTIVE
     *
     * @param bookingId
     * @param reservationRequest
     * @param result
     * @return a specific ReservationEntity updated
     */
    public ReservationEntity updateReservation(final String bookingId, final ReservationRequest reservationRequest, final BindingResult result) {
        final ReservationEntity reservationEntityUpdate = getReservation(bookingId, Boolean.TRUE);

        reservationRequest.setFullName(reservationEntityUpdate.getUser().getFullName());
        reservationRequest.setEmail(reservationEntityUpdate.getUser().getEmail());
        reservationValidator.validateInput(reservationRequest, result);

        reservationEntityUpdate.setArrivalDate(reservationRequest.getArrivalDate());
        reservationEntityUpdate.setDepartureDate(reservationRequest.getDepartureDate());
        return reservationRepository.saveAndFlush(reservationEntityUpdate);
    }

    /**
     * Create a new reservation based on ReservationRequest dto
     *
     * @param reservationRequest
     * @param result
     * @return a specific ReservationEntity created
     */
    public ReservationEntity createReservation(final ReservationRequest reservationRequest, final BindingResult result) {
        reservationValidator.validateInput(reservationRequest, result);

        //Check if user is already in DB
        UserEntity userEntity = userRepository.findByEmail(reservationRequest.getEmail());
        if (userEntity != null) {
            final List<ReservationEntity> reservationEntityList = reservationRepository.findAllByUserId(userEntity.getId());

            //if the user already has an active reservation then bad request
            if (!reservationEntityList.isEmpty() &&
                    reservationEntityList.stream().anyMatch(r -> checkReservationStatus(r, ReservationStatus.ACTIVE)))
                reservationValidator.badRequestResponse("reservation.invalid.user.active", reservationRequest.getEmail());
        } else {
            userEntity = new UserEntity(reservationRequest.getFullName(), reservationRequest.getEmail());
            userRepository.saveAndFlush(userEntity);
        }

        final ReservationBuilder reservationBuilder = ReservationEntity.getBuilder();

        reservationBuilder
                .withUser(userEntity)
                .withArrivalDate(reservationRequest.getArrivalDate())
                .withDepartureDate(reservationRequest.getDepartureDate());

        return reservationRepository.saveAndFlush(reservationBuilder.build());
    }
}
