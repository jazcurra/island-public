/*
 * Copyright (c) 2019.
 * Javier Azcurra - jazcurra.it@gmail.com
 */

package com.company.island.service;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * @author Javier Azcurra
 * <p>
 * This Date Service contains the date operations related to business logic
 */

@Service
public class DateService {

    /**
     * These values are set from application.properties
     */
    @Value("${island.days.min.ahead.reserve:1}")
    private int minDaysAheadReserve;

    @Value("${island.months.max.arrival:1}")
    private int maxMonthsArrival;

    @Value("${island.days.max.reserve:3}")
    private int maxDaysReserve;

    public int getMinDaysAheadReserve() {
        return minDaysAheadReserve;
    }

    public int getMaxMonthsArrival() {
        return maxMonthsArrival;
    }

    public int getMaxDaysReserve() {
        return maxDaysReserve;
    }

    /**
     * Return current Date truncating the hour - setting the hour = 0
     *
     * @return a Calendar.
     */
    private static Calendar getCalendarTruncated() {
        final Calendar c = Calendar.getInstance();
        c.setTime(DateUtils.truncate(new Date(), Calendar.DATE));
        return c;
    }

    /**
     * Return current Date adding the min number of day(s) ahead from reserve
     *
     * @return a Date.
     */
    public Date getMinDateOfArrival() {
        final Calendar c = getCalendarTruncated();
        c.add(Calendar.DATE, minDaysAheadReserve);
        return c.getTime();
    }

    /**
     * Return current Date adding the number of month(s) and discounting one day of reserve
     *
     * @return a Date.
     */
    public Date getMaxDateOfArrival() {
        final Calendar c = getCalendarTruncated();
        c.add(Calendar.MONTH, maxMonthsArrival);
        c.add(Calendar.DATE, -1);
        return c.getTime();
    }

    /**
     * Return current Date adding 1 day of selected arrival date
     *
     * @return a Date.
     */
    public Date getMinDateDepartureOfArrival(final Date arrival) {
        final Calendar c = Calendar.getInstance();
        c.setTime(arrival);
        c.add(Calendar.DATE, 1);
        return c.getTime();
    }

    /**
     * Return current Date adding value of max days to reserve
     *
     * @return a Date.
     */
    public Date getMaxDateDepartureOfArrival(final Date arrival) {
        final Calendar c = Calendar.getInstance();
        c.setTime(arrival);
        c.add(Calendar.DATE, maxDaysReserve);

        final Date maxDateOfDeparture = getMaxDateOfDeparture();
        if (!c.getTime().before(maxDateOfDeparture))
            return maxDateOfDeparture;

        return c.getTime();
    }

    /**
     * Return current Date adding the number of max month(s)
     *
     * @return a Date.
     */
    public Date getMaxDateOfDeparture() {
        final Calendar c = getCalendarTruncated();
        c.add(Calendar.MONTH, maxMonthsArrival);
        return c.getTime();
    }

    /**
     * Check arrival date is before departure date
     *
     * @param arrivalDate
     * @param departureDate
     * @return <code>true</code> if and only if the instant of time
     * represented by arrivalDate object is strictly
     * earlier than the instant represented by departureDate
     * <code>false</code> otherwise.
     */
    public boolean checkArrivalBeforeDeparture(final Date arrivalDate, final Date departureDate) {
        return arrivalDate.before(departureDate);
    }

    /**
     * Check arrival date is after or equals (>=) the days min of reserve
     *
     * @param arrivalDate
     * @return <code>true</code> if and only if the instant of time
     * represented by this arrivalDate object is
     * later or equals than the instant represented by getMinDateOfArrival()
     * <code>false</code> otherwise.
     */
    public boolean checkArrivalAfterMinTimeReserve(final Date arrivalDate) {
        return !arrivalDate.before(getMinDateOfArrival());
    }

    /**
     * Check arrival date is before or equals (<=) the days max of reserve
     *
     * @param arrivalDate
     * @return <code>true</code> if and only if the instant of time
     * represented by this arrivalDate object is
     * earlier or equals than the instant represented by getMaxDateOfArrival()
     * <code>false</code> otherwise.
     */
    public boolean checkArrivalBeforeMaxTimeReserve(final Date arrivalDate) {
        return !arrivalDate.after(getMaxDateOfArrival());
    }

    /**
     * Check if departure date is before or equals (<=) the day Max of reserve
     *
     * @param departureDate
     * @return <code>true</code> if and only if the instant of time
     * represented by this departureDate object is
     * earlier or equals than the instant represented by getMaxDateOfDeparture()
     * <code>false</code> otherwise.
     */
    public boolean checkDepartureBeforeMaxTimeReserve(final Date departureDate) {
        return !departureDate.after(getMaxDateOfDeparture());
    }

    /**
     * Check if days are inside max diff between Arrival and  Departure date
     *
     * @param arrivalDate
     * @param departureDate
     * @return <code>true</code> if and only if the number of days
     * between arrivalDate and departureDate
     * is inside the inclusive range [1,maxDaysReserve]
     * <code>false</code> otherwise.
     */
    public boolean checkBetweenArrivalDepartureDiffMaxDays(final Date arrivalDate, final Date departureDate) {
        final long diffInMillis = departureDate.getTime() - arrivalDate.getTime();
        final long diffInDays = TimeUnit.MILLISECONDS.toDays(diffInMillis);

        return (diffInDays >= 1 && diffInDays <= maxDaysReserve);
    }
}
