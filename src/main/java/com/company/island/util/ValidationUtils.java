/*
 * Copyright (c) 2019.
 * Javier Azcurra - jazcurra.it@gmail.com
 */

package com.company.island.util;

import org.apache.commons.validator.routines.EmailValidator;
import org.apache.logging.log4j.util.Strings;

import java.util.Arrays;

/**
 * @author Javier Azcurra
 * <p>
 * Validation Utils Class
 */

public class ValidationUtils {

    private static EmailValidator emailValidator = EmailValidator.getInstance();

    /**
     * Normalize the input string removing the blanks and convert all chars in lowercase
     *
     * @param input
     * @return
     */
    public static String normalizeString(String input) {
        if (Strings.isBlank(input))
            return input;

        return input.trim().toLowerCase();
    }

    /**
     * Return an array containing the input objects
     *
     * @param var
     * @return
     */
    public static Object[] wrapArray(Object... var) {
        return Arrays.stream(var).toArray();
    }

    /**
     * Checks if the email format is correct
     *
     * @param email
     * @return
     */
    public static boolean validateEmailFormat(String email) {
        return emailValidator.isValid(email);
    }
}
