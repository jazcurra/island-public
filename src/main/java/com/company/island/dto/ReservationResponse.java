/*
 * Copyright (c) 2019.
 * Javier Azcurra - jazcurra.it@gmail.com
 */

package com.company.island.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;

/**
 * @author Javier Azcurra
 * <p>
 * Dto response of Reservation Response
 */

public class ReservationResponse {

    private String bookingId;
    private Date arrivalDate;
    private Date departureDate;
    private String fullName;
    private String email;
    private String status;

    @JsonGetter
    public String getBookingId() {
        return bookingId;
    }

    @JsonIgnore
    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    @JsonGetter
    @JsonFormat(pattern = "yyyy-MM-dd")
    public Date getArrivalDate() {
        return arrivalDate;
    }

    @JsonIgnore
    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    @JsonGetter
    @JsonFormat(pattern = "yyyy-MM-dd")
    public Date getDepartureDate() {
        return departureDate;
    }

    @JsonGetter
    public String getFullName() {
        return fullName;
    }

    @JsonIgnore
    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    @JsonIgnore
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @JsonGetter
    public String getEmail() {
        return email;
    }

    @JsonIgnore
    public void setEmail(String email) {
        this.email = email;
    }

    @JsonGetter
    public String getStatus() {
        return status;
    }

    @JsonIgnore
    public void setStatus(String status) {
        this.status = status;
    }
}
