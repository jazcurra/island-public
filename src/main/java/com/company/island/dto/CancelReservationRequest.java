/*
 * Copyright (c) 2019.
 * Javier Azcurra - jazcurra.it@gmail.com
 */

package com.company.island.dto;

import com.fasterxml.jackson.annotation.JsonSetter;

/**
 * @author Javier Azcurra
 * <p>
 * Dto response of Cancel Reservation Request
 */

public class CancelReservationRequest {

    private Boolean cancel;

    @JsonSetter
    public void setCancel(Boolean cancel) {
        this.cancel = cancel;
    }

    public Boolean getCancel() {
        return cancel;
    }
}
