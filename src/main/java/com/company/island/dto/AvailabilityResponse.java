/*
 * Copyright (c) 2019.
 * Javier Azcurra - jazcurra.it@gmail.com
 */

package com.company.island.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;

/**
 * @author Javier Azcurra
 * <p>
 * Dto response Availability Date Ranges
 */

public class AvailabilityResponse {

    private Date startDate;
    private Date endDate;

    public AvailabilityResponse(Date startDate, Date endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }

    @JsonGetter
    @JsonFormat(pattern = "yyyy-MM-dd")
    public Date getStartDate() {
        return startDate;
    }

    @JsonGetter
    @JsonFormat(pattern = "yyyy-MM-dd")
    public Date getEndDate() {
        return endDate;
    }

    @JsonIgnore
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @JsonIgnore
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
