/*
 * Copyright (c) 2019.
 * Javier Azcurra - jazcurra.it@gmail.com
 */

package com.company.island.dto;

import com.fasterxml.jackson.annotation.JsonSetter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author Javier Azcurra
 * <p>
 * Dto response of Reservation Request
 */

public class ReservationRequest {

    private String fullName;
    private String email;
    private Date arrivalDate;
    private Date departureDate;

    @JsonSetter
    public void setFullName(final String fullName) {
        this.fullName = fullName;
    }

    @JsonSetter
    public void setEmail(final String email) {
        this.email = email;
    }

    @JsonSetter
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public void setArrivalDate(final Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    @JsonSetter
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public void setDepartureDate(final Date departureDate) {
        this.departureDate = departureDate;
    }

    public String getFullName() {
        return fullName;
    }

    public String getEmail() {
        return this.email;
    }

    public Date getArrivalDate() {
        return this.arrivalDate;
    }

    public Date getDepartureDate() {
        return this.departureDate;
    }
}
