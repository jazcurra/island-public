/*
 * Copyright (c) 2019.
 * Javier Azcurra - jazcurra.it@gmail.com
 */

package com.company.island.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;
import java.util.StringJoiner;

/**
 * @author Javier Azcurra
 * <p>
 * User entity class
 */

@Entity
@Table(name = "user")
public class UserEntity extends AbstractPerson {

    public UserEntity(String fullName, String email) {
        super(fullName, email);
    }

    public UserEntity() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserEntity)) return false;
        UserEntity user = (UserEntity) o;
        return Objects.equals(getEmail(), user.getEmail());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getEmail());
    }


    @Override
    public String toString() {
        return new StringJoiner(", ", UserEntity.class.getSimpleName() + "[", "]")
                .add("fullName='" + getFullName() + "'")
                .add("email='" + getEmail() + "'")
                .toString();
    }
}
