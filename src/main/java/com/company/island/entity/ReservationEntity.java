/*
 * Copyright (c) 2019.
 * Javier Azcurra - jazcurra.it@gmail.com
 */

package com.company.island.entity;

import com.company.island.builder.ReservationBuilder;
import com.company.island.entity.enumeration.ReservationStatus;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.logging.log4j.util.Strings;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/**
 * @author Javier Azcurra
 * <p>
 * Reservation entity class
 */

@Entity
@Table(name = "reservation")
public class ReservationEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String bookingId;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private UserEntity user;

    @Column(nullable = false)
    @Temporal(TemporalType.DATE)
    private Date arrivalDate;

    @Column(nullable = false)
    @Temporal(TemporalType.DATE)
    private Date departureDate;

    @Column(nullable = false)
    @Temporal(TemporalType.DATE)
    private Date creationDate;

    @Temporal(TemporalType.DATE)
    private Date updateDate;

    @Temporal(TemporalType.DATE)
    private Date cancelDate;

    @Column(nullable = false)
    private Boolean cancelled;

    @PrePersist
    public void onCreate() {
        bookingId = UUID.randomUUID().toString().replace("-", Strings.EMPTY);
        creationDate = new Date();
        cancelled = false;
    }

    @PreUpdate
    public void onUpdate() {
        updateDate = new Date();
        if (cancelled)
            cancelDate = new Date();
    }

    public Long getId() {
        return id;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Date getCancelDate() {
        return cancelDate;
    }

    public void setCancelDate(Date cancelDate) {
        this.cancelDate = cancelDate;
    }

    public Boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(Boolean cancelled) {
        this.cancelled = cancelled;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ReservationEntity)) return false;
        ReservationEntity that = (ReservationEntity) o;
        return Objects.equals(bookingId, that.bookingId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bookingId);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ReservationEntity.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("bookingId='" + bookingId + "'")
                .add("user=" + user)
                .add("arrivalDate=" + arrivalDate)
                .add("departureDate=" + departureDate)
                .add("creationDate=" + creationDate)
                .add("updateDate=" + updateDate)
                .add("cancelDate=" + cancelDate)
                .add("cancelled=" + cancelled)
                .toString();
    }

    public static ReservationBuilder getBuilder() {
        return new ReservationBuilder();
    }

    /**
     * Return the status of the current reservation
     *
     * @return {@code CANCELLED} if cancelled attribute is true.
     * {@code EXPIRED} if arrival date is equals or earlier than the current date.
     * {@code ACTIVE} if is not cancelled and is not expired.
     */
    public ReservationStatus getReservationStatus() {
        final ReservationStatus reservationStatus;

        if (this.isCancelled())
            reservationStatus = ReservationStatus.CANCELLED;
        else if (!DateUtils.truncate(new Date(), Calendar.DATE).before(this.getArrivalDate()))
            reservationStatus = ReservationStatus.EXPIRED;
        else
            reservationStatus = ReservationStatus.ACTIVE;

        return reservationStatus;
    }

    /**
     * Return the status value of the current reservation
     *
     * @return String
     */
    public String getReservationStatusValue() {
        return getReservationStatus().toString();
    }
}
