/*
 * Copyright (c) 2019.
 * Javier Azcurra - jazcurra.it@gmail.com
 */

package com.company.island.entity.enumeration;

/**
 * @author Javier Azcurra
 * <p>
 * Reservation status enumeration type
 */

public enum ReservationStatus {

    ACTIVE("Active"),
    EXPIRED("Expired"),
    CANCELLED("Cancelled");

    public final String status;

    ReservationStatus(final String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return this.status;
    }

    public boolean equals(ReservationStatus reservationStatus) {
        return this == reservationStatus;
    }
}
