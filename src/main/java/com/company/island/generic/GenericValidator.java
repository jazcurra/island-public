/*
 * Copyright (c) 2019.
 * Javier Azcurra - jazcurra.it@gmail.com
 */

package com.company.island.generic;

import com.company.island.exception.BadRequestException;
import com.company.island.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import java.util.Locale;
import java.util.stream.Collectors;

/**
 * @author Javier Azcurra
 * <p>
 * Generic Validator Class
 */

@Component
public abstract class GenericValidator implements Validator {

    /**
     * This value is set from application.properties
     */
    @Value("${string.length.max:90}")
    protected int stringLengthMax;

    @Autowired
    private MessageSource messageSource;

    @Override
    public boolean supports(Class clazz) {
        return getClass().equals(clazz);
    }

    protected void returnBadRequest(BindingResult result) {
        if (result.hasErrors()) {
            final String resultErrors = result.getAllErrors()
                    .stream()
                    .map(x -> getMessageSource(x.getDefaultMessage(), x.getArguments()))
                    .collect(Collectors.joining(", "));

            throw new BadRequestException(resultErrors);
        }
    }

    public void validateInput(Object o, BindingResult result) {
        validate(o, result);
        returnBadRequest(result);
    }

    protected String getMessageSource(String message, Object... arguments) {
        return messageSource.getMessage(message, arguments, Locale.getDefault());
    }

    public void notFoundResponse(String message, Object... arguments) {
        throw new NotFoundException(getMessageSource(message, arguments));
    }

    public void badRequestResponse(String message, Object... arguments) {
        throw new BadRequestException(getMessageSource(message, arguments));
    }
}
