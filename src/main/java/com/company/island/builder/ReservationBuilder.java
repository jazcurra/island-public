/*
 * Copyright (c) 2019.
 * Javier Azcurra - jazcurra.it@gmail.com
 */

package com.company.island.builder;

import com.company.island.entity.ReservationEntity;
import com.company.island.entity.UserEntity;
import java.util.Date;

/**
 * @author  Javier Azcurra
 *
 * Builder of {@link ReservationEntity}
 */

public final class ReservationBuilder {

    private ReservationEntity reservation;

    public ReservationBuilder() {
        this.reservation = new ReservationEntity();
    }

    public ReservationBuilder withUser(final UserEntity user) {
        this.reservation.setUser(user);
        return this;
    }

    public ReservationBuilder withBookingId(final String bookingId) {
        this.reservation.setBookingId(bookingId);
        return this;
    }

    public ReservationBuilder withArrivalDate(final Date arrivalDate) {
        this.reservation.setArrivalDate(arrivalDate);
        return this;
    }

    public ReservationBuilder withDepartureDate(final Date departureDate) {
        this.reservation.setDepartureDate(departureDate);
        return this;
    }

    public ReservationBuilder withCreationDate(final Date creationDate) {
        this.reservation.setCreationDate(creationDate);
        return this;
    }

    public ReservationBuilder withUpdateDate(final Date updateDate) {
        this.reservation.setUpdateDate(updateDate);
        return this;
    }

    public ReservationBuilder withCancelDate(final Date cancelDate) {
        this.reservation.setCancelDate(cancelDate);
        return this;
    }

    public ReservationBuilder isCancelled(final Boolean cancelled) {
        this.reservation.setCancelled(cancelled);
        return this;
    }

    public ReservationEntity build() {
        return this.reservation;
    }
}
