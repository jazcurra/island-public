/*
 * Copyright (c) 2019.
 * Javier Azcurra - jazcurra.it@gmail.com
 */

package com.company.island.validator;


import com.company.island.dto.ReservationRequest;
import com.company.island.generic.GenericValidator;
import com.company.island.service.DateService;
import com.company.island.util.ValidationUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

/**
 * @author Javier Azcurra
 * <p>
 * Reservation Validator Class
 */

@Component
public class ReservationValidator extends GenericValidator {

    @Autowired
    private DateService dateService;

    @Override
    public void validate(Object target, Errors errors) {
        final ReservationRequest reservationRequest = (ReservationRequest) target;

        if (Strings.isBlank(reservationRequest.getFullName())) {
            errors.reject("fullName", "fullName.notEmpty");
        } else if (reservationRequest.getFullName().length() > stringLengthMax) {
            errors.reject("fullName", ValidationUtils.wrapArray(stringLengthMax), "fullName.tooLong");
        } else {
            reservationRequest.setFullName(reservationRequest.getFullName().trim());
        }

        if (Strings.isBlank(reservationRequest.getEmail())) {
            errors.reject("email", "email.notEmpty");
        } else if (reservationRequest.getEmail().length() > stringLengthMax) {
            errors.reject("email", ValidationUtils.wrapArray(stringLengthMax), "email.tooLong");
        } else if (!ValidationUtils.validateEmailFormat(reservationRequest.getEmail())) {
            errors.reject("email", ValidationUtils.wrapArray(reservationRequest.getEmail()), "email.invalid");
        } else {
            reservationRequest.setEmail(ValidationUtils.normalizeString(reservationRequest.getEmail()));
        }

        if (reservationRequest.getArrivalDate() == null) {
            errors.reject("arrivalDate", "arrivalDate.notEmpty");
        }

        if (reservationRequest.getDepartureDate() == null) {
            errors.reject("departureDate", "departureDate.notEmpty");
        }

        if (reservationRequest.getArrivalDate() == null || reservationRequest.getDepartureDate() == null) {
            return;
        }

        if (!dateService.checkArrivalAfterMinTimeReserve(reservationRequest.getArrivalDate())) {
            errors.reject("arrivalDate", ValidationUtils.wrapArray(dateService.getMinDateOfArrival()), "arrivalDate.min.time.availability.daterange");
        }

        if (!dateService.checkArrivalBeforeMaxTimeReserve(reservationRequest.getArrivalDate())) {
            errors.reject("arrivalDate", ValidationUtils.wrapArray(dateService.getMaxDateOfArrival()), "arrivalDate.max.time.availability.daterange");
            return;
        }

        if (!dateService.checkArrivalBeforeDeparture(reservationRequest.getArrivalDate(), reservationRequest.getDepartureDate())) {
            errors.reject("arrivalDate", "arrivalDate.before.departureDate");
            return;
        }

        if (!dateService.checkDepartureBeforeMaxTimeReserve(reservationRequest.getDepartureDate())) {
            errors.reject("departureDate", ValidationUtils.wrapArray(dateService.getMaxMonthsArrival()), "departureDate.max.time.reserve");
        }

        if (!dateService.checkBetweenArrivalDepartureDiffMaxDays(reservationRequest.getArrivalDate(), reservationRequest.getDepartureDate())) {
            errors.reject("dateDiffMaxLimit", ValidationUtils.wrapArray(dateService.getMaxDaysReserve()), "dateDiff.max.limit");
        }
    }
}
