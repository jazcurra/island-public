/*
 * Copyright (c) 2019.
 * Javier Azcurra - jazcurra.it@gmail.com
 */

package com.company.island.mapper;

import com.company.island.dto.ReservationRequest;
import com.company.island.dto.ReservationResponse;
import com.company.island.entity.ReservationEntity;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;
import java.util.List;

/**
 * @author Javier Azcurra
 * <p>
 * Mapper of reservation entity with dto objects
 */

@Component
public class ReservationMapper extends ModelMapper {

    private final static Type reservationListType = new TypeToken<List<ReservationResponse>>() {
    }.getType();

    private final static PropertyMap<ReservationEntity, ReservationResponse> reservationResponseMap = new PropertyMap<ReservationEntity, ReservationResponse>() {
        protected void configure() {
            map().setFullName(source.getUser().getFullName());
            map().setEmail(source.getUser().getEmail());
            map().setStatus(source.getReservationStatusValue());
        }
    };

    private final static PropertyMap<ReservationEntity, ReservationRequest> reservationRequestMap = new PropertyMap<ReservationEntity, ReservationRequest>() {
        protected void configure() {
            map().setFullName(source.getUser().getFullName());
            map().setEmail(source.getUser().getEmail());
        }
    };

    public ReservationMapper() {
        this.addMappings(reservationRequestMap);
        this.addMappings(reservationResponseMap);
    }

    public ReservationRequest convertRequest(final ReservationEntity reservationEntity) {
        return this.map(reservationEntity, ReservationRequest.class);
    }

    public ReservationResponse convertResponse(final ReservationEntity reservationEntity) {
        return this.map(reservationEntity, ReservationResponse.class);
    }

    public List<ReservationResponse> convertListResponse(final List<ReservationEntity> reservationEntityList) {
        return this.map(reservationEntityList, reservationListType);
    }
}
