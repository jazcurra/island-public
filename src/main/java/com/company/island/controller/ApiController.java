/*
 * Copyright (c) 2019.
 * Javier Azcurra - jazcurra.it@gmail.com
 */

package com.company.island.controller;

import com.company.island.dto.AvailabilityResponse;
import com.company.island.dto.CancelReservationRequest;
import com.company.island.dto.ReservationRequest;
import com.company.island.entity.ReservationEntity;
import com.company.island.mapper.ReservationMapper;
import com.company.island.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * @author Javier Azcurra
 * <p>
 * Backend Rest controller
 */

@RestController
@RequestMapping("/api/reservations")
public class ApiController {

    @Autowired
    ReservationService reservationService;

    @Autowired
    ReservationMapper reservationMapper;

    @GetMapping(value = "/{bookingId}")
    public ResponseEntity getReservation(@PathVariable("bookingId") String bookingId) {
        final ReservationEntity reservationEntity = reservationService.getReservation(bookingId);
        return ResponseEntity
                .ok()
                .body(reservationMapper.convertResponse(reservationEntity));
    }

    @GetMapping()
    public ResponseEntity getReservations(@RequestParam(value = "full_name", required = false) final String fullName,
                                         @RequestParam(value = "email", required = false) final String email) {
        final List<ReservationEntity> reservationEntityList = reservationService.searchReservation(fullName, email);
        return ResponseEntity
                .ok()
                .body(reservationMapper.convertListResponse(reservationEntityList));
    }

    @GetMapping(value = "/availability")
    public ResponseEntity getAvailability(@RequestParam(value = "date_type", required = false) final String dateType,
                                          @RequestParam(value = "arrival_date", required = false)
                                          @DateTimeFormat(pattern = "yyyy-MM-dd") final Date arrivalDate) {
        final AvailabilityResponse availabilityResponse = reservationService.getAvailabilityDateRange(dateType, arrivalDate);
        return ResponseEntity
                .ok()
                .body(availabilityResponse);
    }

    @PatchMapping(value = "/{bookingId}")
    public ResponseEntity cancelReservation(@PathVariable("bookingId") String bookingId, @RequestBody CancelReservationRequest cancelReservationRequest) {
        final ReservationEntity reservationCancelled = reservationService.cancelReservation(bookingId, cancelReservationRequest.getCancel());
        return ResponseEntity
                .ok()
                .body(reservationMapper.convertResponse(reservationCancelled));
    }

    @PutMapping(value = "/{bookingId}")
    public ResponseEntity updateReservation(@PathVariable("bookingId") String bookingId, @RequestBody ReservationRequest reservationRequest, BindingResult result) {
        final ReservationEntity reservationUpdated = reservationService.updateReservation(bookingId, reservationRequest, result);
        return ResponseEntity
                .ok()
                .body(reservationMapper.convertResponse(reservationUpdated));
    }

    @PostMapping()
    public ResponseEntity createReservation(@RequestBody ReservationRequest reservationRequest, BindingResult result) {
        final ReservationEntity reservationCreated = reservationService.createReservation(reservationRequest, result);
        return new ResponseEntity<>(reservationMapper.convertResponse(reservationCreated), null, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{bookingId}")
    public ResponseEntity deleteReservation(@PathVariable("bookingId") String bookingId) {
        reservationService.deleteReservation(bookingId);
        return ResponseEntity.noContent().build();
    }
}
