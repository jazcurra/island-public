/*
 * Copyright (c) 2019.
 * Javier Azcurra - jazcurra.it@gmail.com
 */

package com.company.island.controller;

import com.company.island.dto.ReservationRequest;
import com.company.island.dto.ReservationResponse;
import com.company.island.entity.ReservationEntity;
import com.company.island.mapper.ReservationMapper;
import com.company.island.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

/**
 * @author Javier Azcurra
 * <p>
 * User Interface controller
 */

@Controller
@RequestMapping("/ui/reservations")
public class UIController {

    @Autowired
    private ReservationService reservationService;

    @Autowired
    private ReservationMapper reservationMapper;

    @RequestMapping("/list")
    public String list(@RequestParam(value = "full_name", required = false) String fullName, @RequestParam(value = "email", required = false) String email, Model model) {
        final List<ReservationEntity> reservationEntityList = reservationService.searchReservation(fullName, email);
        final List<ReservationResponse> reservationResponseList = reservationMapper.convertListResponse(reservationEntityList);

        model.addAttribute("reservationList", reservationResponseList);
        model.addAttribute("fullName", fullName);
        model.addAttribute("email", email);
        return "/main";
    }

    @RequestMapping("/show")
    public String show() {
        return "/res/show";
    }

    @RequestMapping("/display")
    public String display(@RequestParam("booking_id") String bookingId, @RequestParam("action") String action, Model model) {
        try {
            final ReservationEntity reservationEntity = reservationService.getReservation(bookingId, Boolean.TRUE);
            if (action.equals("edit"))
                model.addAttribute("reservationRequest", reservationMapper.convertRequest(reservationEntity));
            else if (action.equals("cancel"))
                model.addAttribute("reservationResponse", reservationMapper.convertResponse(reservationEntity));
        } catch (ResponseStatusException r) {
            model.addAttribute("errorMsg", r.getReason());
        }
        model.addAttribute("bookingId", bookingId);
        return "/res/" + action;
    }

    @RequestMapping("/edit")
    public String edit() {
        return "/res/edit";
    }

    @RequestMapping("/update")
    public String update(@RequestParam("booking_id") String bookingId, ReservationRequest reservationRequest, Model model, BindingResult result) {
        try {
            final ReservationEntity reservationEntity = reservationService.updateReservation(bookingId, reservationRequest, result);
            model.addAttribute("successMsg", "label.reservation.edited.successfully");
            model.addAttribute("args", reservationEntity.getBookingId());
            model.addAttribute("reservationRequest", null);
        } catch (ResponseStatusException r) {
            model.addAttribute("bookingId", bookingId);
            model.addAttribute("errorMsg", r.getReason());
            model.addAttribute("reservationRequest", reservationRequest);
        }
        return "/res/edit";
    }

    @RequestMapping("/cancel")
    public String cancel() {
        return "/res/cancel";
    }

    @RequestMapping("/revoke")
    public String revoke(@RequestParam("booking_id") String bookingId, Model model) {
        try {
            reservationService.cancelReservation(bookingId, Boolean.TRUE);
            model.addAttribute("successMsg", "label.reservation.cancelled.successfully");
            model.addAttribute("args", bookingId);
        } catch (ResponseStatusException r) {
            model.addAttribute("errorMsg", r.getReason());
            model.addAttribute("bookingId", bookingId);
        }
        return "/res/cancel";
    }

    @RequestMapping("/create")
    public String create(Model model) {
        model.addAttribute("reservationRequest", new ReservationRequest());
        return "/res/create";
    }

    @RequestMapping("/save")
    public String save(@ModelAttribute("reservationRequest") ReservationRequest reservationRequest, Model model, BindingResult result) {
        try {
            final ReservationEntity reservationEntity = reservationService.createReservation(reservationRequest, result);
            model.addAttribute("successMsg", "label.reservation.created.successfully");
            model.addAttribute("args", reservationEntity.getBookingId());
            model.addAttribute("reservationRequest", new ReservationRequest());
        } catch (ResponseStatusException r) {
            model.addAttribute("errorMsg", r.getReason());
        }
        return "/res/create";
    }
}
