/*
 * Copyright (c) 2019.
 * Javier Azcurra - jazcurra.it@gmail.com
 */

package com.company.island.repository;

import com.company.island.entity.ReservationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Javier Azcurra
 * <p>
 * JPA Reservation repository
 */

@Repository
public interface ReservationRepository extends JpaRepository<ReservationEntity, Long> {

    ReservationEntity findByBookingId(String bookingId);

    List<ReservationEntity> findAllByUserId(long userId);

    List<ReservationEntity> findByUserFullNameStartingWithIgnoreCaseAndUserEmailStartingWith(String fullName, String email);
}