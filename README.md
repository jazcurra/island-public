# Island API

Author: Javier Azcurra

Year: 2019

Mail: jazcurra.it@gmail.com

LinkedIn: https://www.linkedin.com/in/javierazcurra

## Introduction

Welcome!! This simple guide describes the main features and how works this API. I called it Island API because it contains CRUD operations related to campsite reservations into a Caribbean Island.

I tried to implement it with good test coverage, Object Oriented appropriate practices, some design patterns and of course a robust code covered by unit tests to avoid the possible bugs. Let's start:

**Technology employed:**

* Java 8
* Apache Maven Framework
* Spring Boot Framework 2.0.8
* H2 Database (Embedded)
* Apache Tomcat (Embedded)

## Architecture:

![Image](architecture.jpeg)

_(From Left to Right)_

1) The UI (User Interface) clients send a request (from any browser) to Island API against UI controller. After the API process the reservation request, its response to the browser client using UI controller. It is a typical MVC architecture.

2) Similar as point #1 the REST (REpresentational State Transfer) clients can send a request to the Rest controller, but in this case, they will send a standard message and the Rest controller will response in the same standard way. It is a REST architecture.

3) Both controllers above mentioned interacts with Reservation Service. This service contains the main reservations logic. Inside it, there are two modules. Validation Module and Date Service.

4) The Validation Module is executed every time a new input data is sending from one of both controllers. The Date Service contains the business logic related to reservation dates. Also, the Date Service is used by the Validation Module to verify the reservations date inputs.

5) The Reservation Service executes queries against  H2 Database using JPA (Java Persistence API). This queries can retrieve and persist the information from the database.


**Some considerations:**

1) The Island API contains a H2 memory Database. Some reservations data is provided when the server starts. However, if the server is restarted or stopped all the new data entered will be lost.

2) The reservations contain a status property, this could be: Active, Cancelled, Expired.

  * Cancelled: When an active reservation was cancelled by the user.
  * Expired: When the arrival date of an active reservation is equals or earlier than the current date.
  * Active: When the reservation is not cancelled and is not expired.

3) A user with a specific email cannot have more than one Active reservation.

4) A user only can update or cancel a reservation if the status is Active.


## How to run the API?

In a terminal, run the maven command over provided island path folder:

**\> mvn clean spring-boot:run**

If no error is showed the server must start.

Once the server started, this is the main UI URL and it can be accessed for any browser:

http://localhost:8080/island/ui/reservations/list

_The UI is very simple and intuitive and I will not describe in this document._

## Backend - Restful Services:

#### These are the Rest Service Operations:

1- Get one reservation:

Method: GET

Url: http://localhost:8080/island/api/reservations/{booking_id}

**Expected Status code: 200 OK**

============================================

2- Search reservation:

Method: GET

Url: http://localhost:8080/island/api/reservations

_To filter by full name and email add this query string to the URL:_

**?full_name=<any name>&email=<any email>**

_You can filter with starting string values (like% query)_

**Expected Status code: 200 OK**

=============================================

3- Get Availability Date Ranges:

Method: GET

_This date ranges belongs to Arrival Date_
Url: http://localhost:8080/island/api/reservations/availability

_This date ranges belongs to Departure Date, arrival date input required_
Url: http://localhost:8080/island/api/reservations/availability?date_type=departure&arrival_date={arrival_date_selected}

**Expected Status code: 200 OK**

============================================

4- Create reservation:

Method: POST

Url: http://localhost:8080/island/api/reservations

Request Body:

_Date Format: yyyy-MM-dd_

  {
    "fullName": "{full name}",
    "email": "{email}",
    "arrivalDate": "{arrival_date}",
    "departureDate": "{departure_date}"
  }

**Expected Status code: 201 CREATED**

============================================

5- Modify reservation:

Method: PUT

Url: http://localhost:8080/island/api/reservations/{booking_id}

Request Body:

_Date Format: yyyy-MM-dd_

  {
    "arrivalDate": "{arrival_date}",
    "departureDate": "{departure_date}"
  }

**Expected Status code: 200 OK**

============================================

6- Delete reservation:

Method: DELETE

Url: http://localhost:8080/island/api/reservations/{booking_id}

**Expected Status code: 200 OK**

============================================

7- Cancel reservation:

Method: PATCH

Url: http://localhost:8080/island/api/reservations/{booking_id}

Request Body:

  {
    "cancel": "true"
  }

**Expected Status code: 204 NO CONTENT**

============================================

## Testing

In a terminal, run the maven command over island path folder:

**\> mvn test**

_Number of Units test: 108. The code coverage of this unit tests is very close to 100%._
